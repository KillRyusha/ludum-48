using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowStats : MonoBehaviour
{
    private string[] arrowDirs=new string[] {"right","left","up","down" };
    [SerializeField] private GameObject[] arrowImg;
    private Image background;
    [SerializeField] private GameObject backgroundSprite;
    [SerializeField] private GameObject ActiveBackgroundSprite;

    public string ArrowDir;
    public bool IsActive;
    private void OnEnable()
    {
        transform.GetComponent<Animator>().Play("DoNothing");
    }
    void Start()
    {
        background = transform.GetComponent<Image>();
        RandomDir();
    }

    void Update()
    {
        if (IsActive == true)
        {
            ActiveBackgroundSprite.SetActive(true);
            backgroundSprite.SetActive(false);
        }
        else
        {
            ActiveBackgroundSprite.SetActive(false);
            backgroundSprite.SetActive(true);
        }
    }
    public void RandomDir()
    {
        for (int i = 0; i < arrowImg.Length; i++)
        {
            arrowImg[i].SetActive(false);
        }
        int rand = Random.RandomRange(0, 4);
        ArrowDir = arrowDirs[rand];
        arrowImg[rand].SetActive(true);
    }
}
