using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Border : MonoBehaviour
{
    [SerializeField] private bool Ceiling;
    [SerializeField] private Counter Plus;
    
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private AudioSource audiox;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case ("Hedgehog"):
                Destroy(collision.gameObject);
                break;
            case ("Mouse"):
                Destroy(collision.gameObject);
                if (Ceiling)
                {
                    Plus.AnimalCollected(0);
                    particle.Play();
                    audiox.Play();
                }
                    break;
            case ("Rabbit"):
                Destroy(collision.gameObject);
                if (Ceiling)
                {
                    Plus.AnimalCollected(1);
                    particle.Play();
                    audiox.Play();
                }
                break;
            case ("Hog"):
                Destroy(collision.gameObject);
                if (Ceiling)
                {
                    Plus.AnimalCollected(2);
                    particle.Play();
                    audiox.Play();
                }
                break;
            default:
                break;
        }
    }
}
