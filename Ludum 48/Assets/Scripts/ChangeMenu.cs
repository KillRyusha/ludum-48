using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ChangeMenu : MonoBehaviour
{
    bool changer = false;
    public GameObject Menu, Settings;
    public void Change()
    {
        changer = !changer;
        if (changer)
        {
            Menu.SetActive(!changer);
            Settings.SetActive(changer);
        }
        else
        {
            Menu.SetActive(!changer);
            Settings.SetActive(changer);
        }
    }
    public void ClearTutor()
    {
        Settings.SetActive(false);
        Time.timeScale = 1;
    }
}
