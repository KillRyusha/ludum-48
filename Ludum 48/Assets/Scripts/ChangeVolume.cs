using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ChangeVolume : MonoBehaviour
{
    [SerializeField] private AudioMixer mixer;
public void SetSound(float set)
    {
        float value = Mathf.Lerp(-60, 20, set);
        mixer.SetFloat("Sound",value);
        if (value == -60)
            value = -80;
    }
    public void SetMusic(float set)
    {
        float value = Mathf.Lerp(-60, 20, set);
        mixer.SetFloat("Music", Mathf.Lerp(-60, 20, set));
        if (value == -60)
            value = -80;
    }
}
