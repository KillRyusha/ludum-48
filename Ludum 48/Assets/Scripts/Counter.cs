using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Counter : MonoBehaviour
{
    public TMP_Text[] mouseText;
    [SerializeField] private ImageAdder[] count;
    [SerializeField] private int[] points = new int[3];
    [SerializeField] private int AnimalsNeed;
    public int AnimalsCollected;
    [SerializeField] private GameObject winWindow;
    [SerializeField] private AudioSource audio;
    [SerializeField] private Animator anim;
    bool win = false;
    void Start()
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = count[i].amountOfImages;
        }
    }

    void Update()
    {
        if (AnimalsCollected == AnimalsNeed)
        {
            winWindow.SetActive(true);
            if (win == false)
            {
                anim.SetBool("Change", true);
                audio.Play();
                win = true;
            }
            if (!audio.isPlaying)
            {
                Time.timeScale = 0;
                anim.SetBool("Change", false);
            }
        }
    }
    public void AnimalCollected(int value)
    {
        points[value]--;
        count[value].SetValue(points[value]);
        if (points[value] == 0)
        {
            AnimalsCollected++;
        }
    }
}
