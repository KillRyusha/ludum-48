using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour
{
    [Header("���������� ������� ��� �������")]
    [SerializeField] private int mouseArrows;
    [SerializeField] private int rabbitArrows;
    [SerializeField] private int hogArrows;
    [Header("����� ������� ��� �������")]
    [SerializeField] private float mouseTime;
    [SerializeField] private float rabbitTime;
    [SerializeField] private float hogTime;

    public bool isDrop = false;
    public bool canGrab = false;
    Rigidbody2D main;
    public bool isHooked = false;
    public GameObject Timer;
    public SpawnArrows arrowsSpawn;
    [SerializeField] private Timer timer;
    private Walk animal;

    bool canSet = true;
    [SerializeField] private Lives damage;
    private Animator anim;
    [SerializeField] private Transform startPosition;
    [SerializeField] private Transform spawner;
    [SerializeField] private AudioSource[] sound;
    void Start()
    {
        main = gameObject.GetComponent<Rigidbody2D>();
        anim = transform.GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)&& main.bodyType == RigidbodyType2D.Static&&Time.timeScale!=0)
        {
            isDrop = true;
            sound[0].Play();
        }
        if (isDrop)
        {
            main.bodyType = RigidbodyType2D.Dynamic;
        }else
        {
            main.bodyType = RigidbodyType2D.Static;
        }
        if (canGrab == false&&transform.childCount>2)
        {
            Walk Animal = transform.GetChild(transform.childCount-1).transform.GetComponent<Walk>();
            transform.GetChild(transform.childCount-1).transform.parent = null;
            Animal.speed = 1f;
            Animal.transform.position = new Vector3(Animal.transform.position.x, spawner.position.y, 0);
            sound[1].Play();
        }
        }
    public void LaunchAnimal()
    {
        main.AddForce(Vector2.up * 40, ForceMode2D.Impulse);
        anim.SetBool("Hold", false);
        sound[2].Play();
    }

    public void AnimalSetUp(Collider2D obj,int arrows,float time)
    {
        animal = obj.transform.GetComponent<Walk>();
        animal.speed = 0;
        anim.SetBool("Hold", true);
        isHooked = true;
        canGrab = true;
        Timer.SetActive(true);
        arrowsSpawn.amountOfArrows = arrows;
        timer.SetTime(time);
        canSet = true;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Hedgehog" && isHooked == false)
        {
            LaunchAnimal();
            damage.ChangeHealth();
        }
        if (other.tag == "Mouse" && isHooked == false)
        {
            AnimalSetUp(other, mouseArrows, mouseTime);
            StartCoroutine(SetAnimalPaternt(other));
        }
        if (other.tag == "Rabbit" && isHooked == false)
        {
            AnimalSetUp(other, rabbitArrows, rabbitTime);
            StartCoroutine(SetAnimalPaternt(other));
        }
        if (other.tag == "Hog" && isHooked == false)
        {
            AnimalSetUp(other,hogArrows, hogTime);
            StartCoroutine(SetAnimalPaternt(other));
        }
        if(other.tag == "Ceiling")
        {
            main.velocity = Vector3.zero;
            transform.position = startPosition.position;
            canGrab = true;
            isDrop = false;
            isHooked = false;
        }
    }

    public IEnumerator SetAnimalPaternt(Collider2D animal)
    {
        while (canSet)
        {
                yield return new WaitForSeconds(0.3f);
                animal.transform.SetParent(transform);
            canSet = false;
        }
    }
}
  