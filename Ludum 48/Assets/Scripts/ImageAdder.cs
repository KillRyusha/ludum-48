using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageAdder : MonoBehaviour
{
    public int amountOfImages;
    public int amounOfActiveImages;
    [SerializeField] private List<GameObject> images = new List<GameObject>();
    private void Awake()
    {
        amounOfActiveImages = amountOfImages;
    }
    void Start()
    {
        SetValue(amountOfImages);
    }
    public void SetValue(int value)
    {
        if (value >= 0)
        {
            for (int i = 0; i < images.Count; i++)
            {
                if (value - 1 >= i)
                    images[i].SetActive(true);
                else
                    images[i].SetActive(false);
            }
            if(value>1)
            amounOfActiveImages = value;
        }
    }
    public void RemoveImage(int imageValue)
    {
        images[imageValue].transform.GetComponent<Animator>().SetBool("Remove",true);

    }
    void Update()
    {
        
    }
}
