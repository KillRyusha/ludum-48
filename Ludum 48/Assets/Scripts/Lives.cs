using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour
{
    public Sprite 
        green,
        red;
    public Image[] Health;
    public int damage;
    int Heals;
    [SerializeField] private GameObject loseWindow;
    [SerializeField] private AudioSource audio;
    [SerializeField] private Animator anim;
    bool lose = false;
    void Update()
    {
        Heals = Health.Length - damage;
        if (Heals >= 0 && Heals < Health.Length)
        {
            for (int i = 0; i < Health.Length; i++)
            {
                Health[Heals].sprite = red;
            }
        }
        if (Heals <= 0)
        {
            loseWindow.SetActive(true);
            if (lose == false)
            {
                anim.SetBool("Change", true);
                audio.Play();
                lose = true;
            }
            if (!audio.isPlaying)
            {
                Time.timeScale = 0;
                anim.SetBool("Change", false);
            }
        }
    }
    public void ChangeHealth()
    {
        damage++;
    }
}
