using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour
{
    string main;
    public bool Tutor = false;
    void Start()
    {
        main = SceneManager.GetActiveScene().name;
        if (Tutor)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
    void Update()
    {

    }
    public void LoadScene(int value)
    {
        SceneManager.LoadScene(value);
    }
    public void ReLoad()
    {
        SceneManager.LoadScene(main);
    }
    public void GameTime(bool run)
    {
        if (run)
            Time.timeScale = 1;
        else
            Time.timeScale = 0;
    }
}
