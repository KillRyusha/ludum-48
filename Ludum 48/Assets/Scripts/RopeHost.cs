using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeHost : MonoBehaviour
{
    public Transform[] points;
    public  Rope rope;
    void Start()
    {
        rope.SetUpLine(points);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
