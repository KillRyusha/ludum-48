using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpawnArrows : MonoBehaviour
{

    public int amountOfArrows;
    [SerializeField] private List<GameObject> arrows=new List<GameObject>();
    [SerializeField] private List<Animator> arrowsAnims;
    [SerializeField] private int offset;
    [SerializeField] private GameObject arrowPrefab;
    public GameObject canvs;
    [SerializeField]private Timer timer;
    [SerializeField] private Lives lives;
    [SerializeField] private Hook hook;
    [SerializeField] private AudioSource click;
    [SerializeField] private AudioSource falseClick;
    void Start()
    {
    }

    void Update()
    {
        if (Time.timeScale == 1)
        {
            if (amountOfArrows > 0 && amountOfArrows < 11)
            {
                SetLastArrowActive();
                SetAmountOfArrows(amountOfArrows);
                ArrowStats lastArrow = arrows[amountOfArrows - 1].GetComponent<ArrowStats>();
                GetPressedButton(lastArrow.ArrowDir);
            }
            if (hook.isHooked == true)
            {
                if (amountOfArrows <= 0)
                {
                    hook.isDrop = true;
                    canvs.SetActive(false);
                    hook.LaunchAnimal();
                    ResetArrowsValues();
                }
                else if (timer.time <= 0)
                {
                    lives.damage++;
                    canvs.SetActive(false);
                    hook.isDrop = true;
                    hook.canGrab = false;
                    hook.LaunchAnimal();
                    ResetArrowsValues();
                }
            }
            else
                canvs.SetActive(false);
        }

    }

    public void GetPressedButton(string key)
    {
        if (Input.GetKeyDown(KeyCode.RightArrow)&&key=="right")
        {
            amountOfArrows--;
            click.Play();
        }else if (Input.GetKeyDown(KeyCode.LeftArrow) && key == "left")
        {
            amountOfArrows--;
            click.Play();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && key == "up")
        {
            amountOfArrows--;
            click.Play();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && key == "down")
        {
            amountOfArrows--;
            click.Play();
        }
        else if (Input.anyKeyDown && amountOfArrows<10&& !Input.GetMouseButtonDown(0))
        {
            arrows[amountOfArrows - 1].GetComponent<Animator>().SetBool("Move", false);
            arrows[amountOfArrows - 1].GetComponent<Animator>().Play("WronPress");
            amountOfArrows++;
            timer.time += 0.2f;
            falseClick.Play();
        }
    }
    public void SetLastArrowActive()
    {
        for (int i = 0; i < arrows.Count; i++)
        {
            ArrowStats lastArrow = arrows[i].GetComponent<ArrowStats>();
            lastArrow.IsActive = false;
            if (i == amountOfArrows - 1)
            {
                lastArrow.IsActive = true;
                arrows[i].GetComponent<Animator>().SetBool("Move", true);
            }
        }
    }
    public void SetAmountOfArrows(int amount)
    {
        for (int i = 0; i < arrows.Count; i++)
        {
            if (amount-1 >= i)
                arrows[i].SetActive(true);
            else
                arrows[i].SetActive(false);
        }
    }
    public void ResetArrowsValues()
    {
        for (int i = 0; i < arrows.Count; i++)
        {
            ArrowStats arrow = arrows[i].transform.GetComponent<ArrowStats>();
            arrow.RandomDir();
        }
    }
}
