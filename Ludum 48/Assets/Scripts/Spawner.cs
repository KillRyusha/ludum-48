using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] Animals;
    public Walk AnimalWalk;
    public int dir;
    public float Speed;
    public float timer;
    [SerializeField] private ImageAdder[] animals;

    int lastChance = 0;
    [SerializeField] private int[] chance;
    [SerializeField] private List<int> startChance = new List<int>();
    [SerializeField] private int[] spawnTime=new int[2];
    void Start()
    {
        for (int i = 0; i < animals.Length; i++)
        {
            startChance.Add(chance[i]);
        }
        ResetChances();
        SpawnAnimal();
        StartCoroutine(WaitAndPrint(timer));
    }

    void Update()
    {
    }
    private void ResetChances()
    {
        int headgChanse = 0;
        lastChance = 0;
        for (int i = 0; i < animals.Length; i++)
        {
            chance[i] = animals[i].amounOfActiveImages *startChance[i];
        }
        for (int i = 0; i < chance.Length-1; i++)
        {
            headgChanse += chance[i];
        }
        chance[chance.Length-1] = headgChanse / 5;
        for (int i = 0; i < chance.Length; i++)
        {
            lastChance += chance[i];
        }
    }
    private void SpawnAnimal()
    {
        GameObject animal = null;
        
        int rand = Random.Range(0, lastChance + 1);
        if (rand <= chance[0])
        {
            animal = Instantiate(Animals[0]);
        }
        else if (rand > chance[0] && rand <= chance[1]+ chance[0])
        {
            animal = Instantiate(Animals[1]);
        }
        else if (rand > chance[1] + chance[0] && rand <= chance[2] + chance[1] + chance[0])
        {
            animal = Instantiate(Animals[2]);
        }
        else
        {
            animal = Instantiate(Animals[3]);
        }
        animal.transform.position = transform.position;
        animal.transform.localScale = new Vector3(animal.transform.localScale.x * dir, animal.transform.localScale.y, animal.transform.localScale.z);
        AnimalWalk = animal.GetComponent<Walk>();
        AnimalWalk.side = dir;
    }

public IEnumerator WaitAndPrint(float waitTime)
{
    while (true)
    {
        yield return new WaitForSeconds(waitTime);
            SpawnAnimal();
            ResetChances();
            timer = Random.Range(spawnTime[0], spawnTime[1]);
    }
}

}
