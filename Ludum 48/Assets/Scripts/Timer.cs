using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Image main;
    public bool isTime;
    public float time;
    private float localTime;
    [SerializeField] private AudioSource sound;
    private float pitching;
    void Start()
    {
    }
    private void OnEnable()
    {
        sound.Play();
        StartCoroutine(ChangePitch(0.1f));
    }
    private void OnDisable()
    {
        sound.Pause();
        sound.pitch = 1;
        StopCoroutine("ChangePitch");
    }
    public void SetTime(float setTime)
    {
        time = setTime;
        localTime = setTime;
        pitching = time;
        StartCoroutine(TimerStart(0.1f));
    }

    void Update()
    {
        if (isTime == true)
        {
            main.fillAmount = (time/ localTime);
        }
        if (time > localTime)
        {
            time = localTime;
        }
    }
    public IEnumerator ChangePitch(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            sound.pitch = time / pitching;
        }
    }

    public IEnumerator TimerStart(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            time -= 0.1f;
        }
    }
}
