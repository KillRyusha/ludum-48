using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walk : MonoBehaviour
{
    Transform main;
    public float speed;
    public int side;
    [SerializeField] private AudioSource walk;
    void Start()
    {
        main =  gameObject.GetComponent<Transform>();
    }
    private void OnEnable()
    {
        walk.Play();
    }
    private void OnDisable()
    {
        walk.Stop();
    }
    void Update()
    {
        main.position += new Vector3(1,0,0) * speed * side*Time.deltaTime;
        if(Time.timeScale==0)
            walk.Stop();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Hook"&& transform.parent!=null)
        {
                transform.GetComponent<Animator>().SetBool("Caught", true);

        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Hook" && transform.parent != null)
        {
            transform.GetComponent<Animator>().SetBool("Caught", true);
            walk.Stop();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Hook" )
        {
            transform.GetComponent<Animator>().SetBool("Caught", false);
            walk.Play();
        }
    }
}
