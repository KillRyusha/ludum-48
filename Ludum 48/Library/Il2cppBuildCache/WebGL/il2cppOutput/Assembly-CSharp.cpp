﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Collections.Generic.List`1<UnityEngine.Animator>
struct List_1_t8726F3EB67930AF93439C3FE521CAC9E4BE0DD18;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
// ImageAdder[]
struct ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// TMPro.TMP_Text[]
struct TMP_TextU5BU5D_t2735FE650BB383D4F663081E5AA47947843E3676;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// ArrowStats
struct ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// Border
struct Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// ChangeMenu
struct ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641;
// ChangeVolume
struct ChangeVolume_t4B1572DD63F4E337091284BB3DA30C4EFC21A0A1;
// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// Counter
struct Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// Hook
struct Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// ImageAdder
struct ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281;
// UnityEngine.LineRenderer
struct LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967;
// Lives
struct Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// ReloadScene
struct ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// Rope
struct Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8;
// RopeHost
struct RopeHost_t903B27F3928D918D402D4535C94CD5611C450872;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// SpawnArrows
struct SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8;
// Spawner
struct Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// Timer
struct Timer_t32ADF3155A88C37628B80D227C821651666DD405;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// Walk
struct Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029;
// Hook/<SetAnimalPaternt>d__25
struct U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// Spawner/<WaitAndPrint>d__14
struct U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97;
// Timer/<ChangePitch>d__11
struct U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9;
// Timer/<TimerStart>d__12
struct U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E;

IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral01DABAEA1038CE94C68EC1934C99F3FAE1140A8B;
IL2CPP_EXTERN_C String_t* _stringLiteral1051ECAD376DFA7401D0FAA334972924BD291944;
IL2CPP_EXTERN_C String_t* _stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305;
IL2CPP_EXTERN_C String_t* _stringLiteral294F098A1D5E681EDEE8F43BFA8F45CFBA5798A7;
IL2CPP_EXTERN_C String_t* _stringLiteral2BB13E85B9058564A98D5618BDD8518B142BD05E;
IL2CPP_EXTERN_C String_t* _stringLiteral31540315C86289571C1A0ECD739DD7089EB97113;
IL2CPP_EXTERN_C String_t* _stringLiteral378F2A2277727E3F33599E270749A10E7C2B55E1;
IL2CPP_EXTERN_C String_t* _stringLiteral3DEB08645ED85BEE45AC44E1C4021FE85CE537F8;
IL2CPP_EXTERN_C String_t* _stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4;
IL2CPP_EXTERN_C String_t* _stringLiteral7D832450EB8F529B98D1D7829EE23B628D4FD794;
IL2CPP_EXTERN_C String_t* _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2;
IL2CPP_EXTERN_C String_t* _stringLiteral8E4F39EF3084FF922002C13AFD0E988627D2CA29;
IL2CPP_EXTERN_C String_t* _stringLiteral8FCD9DFEDB4B5B0CC0587121C4AB1682B834E22B;
IL2CPP_EXTERN_C String_t* _stringLiteral9984A7A08D209967ED7C21E876406F398FD430C7;
IL2CPP_EXTERN_C String_t* _stringLiteral9D6BA8D36FA648F5F7EC00E75E4E3CB92589DA2E;
IL2CPP_EXTERN_C String_t* _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF;
IL2CPP_EXTERN_C String_t* _stringLiteralB3C34217CBB8F650ED8F6E70B410A604371E2EF1;
IL2CPP_EXTERN_C String_t* _stringLiteralB75428E1AE616B1DD1D25315981C50AB5563C4DE;
IL2CPP_EXTERN_C String_t* _stringLiteralC9A396D4E20D45B3E5DA31508C481845429502A5;
IL2CPP_EXTERN_C String_t* _stringLiteralE917FA4AFDCDAD23459FDCCEB810C2C11F6B1F9C;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_mDB796D00857A64249229F6FC894C95FB0C030A2A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisLineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_mD5BC9EADE1AA529A5299A4D8B020FB49663DAC3A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mDCE712F19A37A792449BA93CB0736F41B4EE17E7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CChangePitchU3Ed__11_System_Collections_IEnumerator_Reset_mB0435CFEF66344023B0C95E4D331B7443E97BB86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_Reset_mB0193444DB856035BD3981DE8F420A1B20BED1F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTimerStartU3Ed__12_System_Collections_IEnumerator_Reset_mB691905ECD7D3D98AD081479657EABBDC25EF95F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_Reset_m5F0E9697F961B13CC552FD329B4005C8AF9F76DE_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B;
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
struct ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____items_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// Hook/<SetAnimalPaternt>d__25
struct  U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835  : public RuntimeObject
{
public:
	// System.Int32 Hook/<SetAnimalPaternt>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Hook/<SetAnimalPaternt>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Collider2D Hook/<SetAnimalPaternt>d__25::animal
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___animal_2;
	// Hook Hook/<SetAnimalPaternt>d__25::<>4__this
	Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_animal_2() { return static_cast<int32_t>(offsetof(U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835, ___animal_2)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_animal_2() const { return ___animal_2; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_animal_2() { return &___animal_2; }
	inline void set_animal_2(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___animal_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animal_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835, ___U3CU3E4__this_3)); }
	inline Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// Spawner/<WaitAndPrint>d__14
struct  U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97  : public RuntimeObject
{
public:
	// System.Int32 Spawner/<WaitAndPrint>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spawner/<WaitAndPrint>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Spawner/<WaitAndPrint>d__14::waitTime
	float ___waitTime_2;
	// Spawner Spawner/<WaitAndPrint>d__14::<>4__this
	Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97, ___U3CU3E4__this_3)); }
	inline Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// Timer/<ChangePitch>d__11
struct  U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9  : public RuntimeObject
{
public:
	// System.Int32 Timer/<ChangePitch>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Timer/<ChangePitch>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Timer/<ChangePitch>d__11::waitTime
	float ___waitTime_2;
	// Timer Timer/<ChangePitch>d__11::<>4__this
	Timer_t32ADF3155A88C37628B80D227C821651666DD405 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9, ___U3CU3E4__this_3)); }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Timer_t32ADF3155A88C37628B80D227C821651666DD405 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// Timer/<TimerStart>d__12
struct  U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E  : public RuntimeObject
{
public:
	// System.Int32 Timer/<TimerStart>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Timer/<TimerStart>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Timer/<TimerStart>d__12::waitTime
	float ___waitTime_2;
	// Timer Timer/<TimerStart>d__12::<>4__this
	Timer_t32ADF3155A88C37628B80D227C821651666DD405 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E, ___U3CU3E4__this_3)); }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Timer_t32ADF3155A88C37628B80D227C821651666DD405 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct  Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.Coroutine
struct  Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.ForceMode2D
struct  ForceMode2D_tAD695DED33FB7C591354430C88D220D71109ABF4 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode2D_tAD695DED33FB7C591354430C88D220D71109ABF4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct  KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RigidbodyType2D
struct  RigidbodyType2D_tEF016AC6D4D54AF47773D68E833159CC995DF1E9 
{
public:
	// System.Int32 UnityEngine.RigidbodyType2D::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RigidbodyType2D_tEF016AC6D4D54AF47773D68E833159CC995DF1E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct  FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct  Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Audio.AudioMixer
struct  AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Sprite
struct  Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.ParticleSystem
struct  ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct  AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Collider2D
struct  Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.LineRenderer
struct  LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// ArrowStats
struct  ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String[] ArrowStats::arrowDirs
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___arrowDirs_4;
	// UnityEngine.GameObject[] ArrowStats::arrowImg
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___arrowImg_5;
	// UnityEngine.UI.Image ArrowStats::background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___background_6;
	// UnityEngine.GameObject ArrowStats::backgroundSprite
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___backgroundSprite_7;
	// UnityEngine.GameObject ArrowStats::ActiveBackgroundSprite
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ActiveBackgroundSprite_8;
	// System.String ArrowStats::ArrowDir
	String_t* ___ArrowDir_9;
	// System.Boolean ArrowStats::IsActive
	bool ___IsActive_10;

public:
	inline static int32_t get_offset_of_arrowDirs_4() { return static_cast<int32_t>(offsetof(ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578, ___arrowDirs_4)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_arrowDirs_4() const { return ___arrowDirs_4; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_arrowDirs_4() { return &___arrowDirs_4; }
	inline void set_arrowDirs_4(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___arrowDirs_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrowDirs_4), (void*)value);
	}

	inline static int32_t get_offset_of_arrowImg_5() { return static_cast<int32_t>(offsetof(ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578, ___arrowImg_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_arrowImg_5() const { return ___arrowImg_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_arrowImg_5() { return &___arrowImg_5; }
	inline void set_arrowImg_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___arrowImg_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrowImg_5), (void*)value);
	}

	inline static int32_t get_offset_of_background_6() { return static_cast<int32_t>(offsetof(ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578, ___background_6)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_background_6() const { return ___background_6; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_background_6() { return &___background_6; }
	inline void set_background_6(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___background_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background_6), (void*)value);
	}

	inline static int32_t get_offset_of_backgroundSprite_7() { return static_cast<int32_t>(offsetof(ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578, ___backgroundSprite_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_backgroundSprite_7() const { return ___backgroundSprite_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_backgroundSprite_7() { return &___backgroundSprite_7; }
	inline void set_backgroundSprite_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___backgroundSprite_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backgroundSprite_7), (void*)value);
	}

	inline static int32_t get_offset_of_ActiveBackgroundSprite_8() { return static_cast<int32_t>(offsetof(ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578, ___ActiveBackgroundSprite_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ActiveBackgroundSprite_8() const { return ___ActiveBackgroundSprite_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ActiveBackgroundSprite_8() { return &___ActiveBackgroundSprite_8; }
	inline void set_ActiveBackgroundSprite_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ActiveBackgroundSprite_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ActiveBackgroundSprite_8), (void*)value);
	}

	inline static int32_t get_offset_of_ArrowDir_9() { return static_cast<int32_t>(offsetof(ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578, ___ArrowDir_9)); }
	inline String_t* get_ArrowDir_9() const { return ___ArrowDir_9; }
	inline String_t** get_address_of_ArrowDir_9() { return &___ArrowDir_9; }
	inline void set_ArrowDir_9(String_t* value)
	{
		___ArrowDir_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ArrowDir_9), (void*)value);
	}

	inline static int32_t get_offset_of_IsActive_10() { return static_cast<int32_t>(offsetof(ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578, ___IsActive_10)); }
	inline bool get_IsActive_10() const { return ___IsActive_10; }
	inline bool* get_address_of_IsActive_10() { return &___IsActive_10; }
	inline void set_IsActive_10(bool value)
	{
		___IsActive_10 = value;
	}
};


// UnityEngine.AudioSource
struct  AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B  : public AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A
{
public:

public:
};


// Border
struct  Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Border::Ceiling
	bool ___Ceiling_4;
	// Counter Border::Plus
	Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * ___Plus_5;
	// UnityEngine.ParticleSystem Border::particle
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particle_6;
	// UnityEngine.AudioSource Border::audiox
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audiox_7;

public:
	inline static int32_t get_offset_of_Ceiling_4() { return static_cast<int32_t>(offsetof(Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6, ___Ceiling_4)); }
	inline bool get_Ceiling_4() const { return ___Ceiling_4; }
	inline bool* get_address_of_Ceiling_4() { return &___Ceiling_4; }
	inline void set_Ceiling_4(bool value)
	{
		___Ceiling_4 = value;
	}

	inline static int32_t get_offset_of_Plus_5() { return static_cast<int32_t>(offsetof(Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6, ___Plus_5)); }
	inline Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * get_Plus_5() const { return ___Plus_5; }
	inline Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D ** get_address_of_Plus_5() { return &___Plus_5; }
	inline void set_Plus_5(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * value)
	{
		___Plus_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Plus_5), (void*)value);
	}

	inline static int32_t get_offset_of_particle_6() { return static_cast<int32_t>(offsetof(Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6, ___particle_6)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_particle_6() const { return ___particle_6; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_particle_6() { return &___particle_6; }
	inline void set_particle_6(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___particle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particle_6), (void*)value);
	}

	inline static int32_t get_offset_of_audiox_7() { return static_cast<int32_t>(offsetof(Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6, ___audiox_7)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audiox_7() const { return ___audiox_7; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audiox_7() { return &___audiox_7; }
	inline void set_audiox_7(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audiox_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audiox_7), (void*)value);
	}
};


// ChangeMenu
struct  ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean ChangeMenu::changer
	bool ___changer_4;
	// UnityEngine.GameObject ChangeMenu::Menu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Menu_5;
	// UnityEngine.GameObject ChangeMenu::Settings
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Settings_6;

public:
	inline static int32_t get_offset_of_changer_4() { return static_cast<int32_t>(offsetof(ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641, ___changer_4)); }
	inline bool get_changer_4() const { return ___changer_4; }
	inline bool* get_address_of_changer_4() { return &___changer_4; }
	inline void set_changer_4(bool value)
	{
		___changer_4 = value;
	}

	inline static int32_t get_offset_of_Menu_5() { return static_cast<int32_t>(offsetof(ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641, ___Menu_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Menu_5() const { return ___Menu_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Menu_5() { return &___Menu_5; }
	inline void set_Menu_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Menu_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Menu_5), (void*)value);
	}

	inline static int32_t get_offset_of_Settings_6() { return static_cast<int32_t>(offsetof(ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641, ___Settings_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Settings_6() const { return ___Settings_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Settings_6() { return &___Settings_6; }
	inline void set_Settings_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Settings_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Settings_6), (void*)value);
	}
};


// ChangeVolume
struct  ChangeVolume_t4B1572DD63F4E337091284BB3DA30C4EFC21A0A1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Audio.AudioMixer ChangeVolume::mixer
	AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * ___mixer_4;

public:
	inline static int32_t get_offset_of_mixer_4() { return static_cast<int32_t>(offsetof(ChangeVolume_t4B1572DD63F4E337091284BB3DA30C4EFC21A0A1, ___mixer_4)); }
	inline AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * get_mixer_4() const { return ___mixer_4; }
	inline AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 ** get_address_of_mixer_4() { return &___mixer_4; }
	inline void set_mixer_4(AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * value)
	{
		___mixer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixer_4), (void*)value);
	}
};


// Counter
struct  Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_Text[] Counter::mouseText
	TMP_TextU5BU5D_t2735FE650BB383D4F663081E5AA47947843E3676* ___mouseText_4;
	// ImageAdder[] Counter::count
	ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* ___count_5;
	// System.Int32[] Counter::points
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___points_6;
	// System.Int32 Counter::AnimalsNeed
	int32_t ___AnimalsNeed_7;
	// System.Int32 Counter::AnimalsCollected
	int32_t ___AnimalsCollected_8;
	// UnityEngine.GameObject Counter::winWindow
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___winWindow_9;
	// UnityEngine.AudioSource Counter::audio
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audio_10;
	// UnityEngine.Animator Counter::anim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___anim_11;
	// System.Boolean Counter::win
	bool ___win_12;

public:
	inline static int32_t get_offset_of_mouseText_4() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___mouseText_4)); }
	inline TMP_TextU5BU5D_t2735FE650BB383D4F663081E5AA47947843E3676* get_mouseText_4() const { return ___mouseText_4; }
	inline TMP_TextU5BU5D_t2735FE650BB383D4F663081E5AA47947843E3676** get_address_of_mouseText_4() { return &___mouseText_4; }
	inline void set_mouseText_4(TMP_TextU5BU5D_t2735FE650BB383D4F663081E5AA47947843E3676* value)
	{
		___mouseText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mouseText_4), (void*)value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___count_5)); }
	inline ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* get_count_5() const { return ___count_5; }
	inline ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81** get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* value)
	{
		___count_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___count_5), (void*)value);
	}

	inline static int32_t get_offset_of_points_6() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___points_6)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_points_6() const { return ___points_6; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_points_6() { return &___points_6; }
	inline void set_points_6(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___points_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_6), (void*)value);
	}

	inline static int32_t get_offset_of_AnimalsNeed_7() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___AnimalsNeed_7)); }
	inline int32_t get_AnimalsNeed_7() const { return ___AnimalsNeed_7; }
	inline int32_t* get_address_of_AnimalsNeed_7() { return &___AnimalsNeed_7; }
	inline void set_AnimalsNeed_7(int32_t value)
	{
		___AnimalsNeed_7 = value;
	}

	inline static int32_t get_offset_of_AnimalsCollected_8() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___AnimalsCollected_8)); }
	inline int32_t get_AnimalsCollected_8() const { return ___AnimalsCollected_8; }
	inline int32_t* get_address_of_AnimalsCollected_8() { return &___AnimalsCollected_8; }
	inline void set_AnimalsCollected_8(int32_t value)
	{
		___AnimalsCollected_8 = value;
	}

	inline static int32_t get_offset_of_winWindow_9() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___winWindow_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_winWindow_9() const { return ___winWindow_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_winWindow_9() { return &___winWindow_9; }
	inline void set_winWindow_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___winWindow_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___winWindow_9), (void*)value);
	}

	inline static int32_t get_offset_of_audio_10() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___audio_10)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audio_10() const { return ___audio_10; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audio_10() { return &___audio_10; }
	inline void set_audio_10(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audio_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audio_10), (void*)value);
	}

	inline static int32_t get_offset_of_anim_11() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___anim_11)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_anim_11() const { return ___anim_11; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_anim_11() { return &___anim_11; }
	inline void set_anim_11(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___anim_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_11), (void*)value);
	}

	inline static int32_t get_offset_of_win_12() { return static_cast<int32_t>(offsetof(Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D, ___win_12)); }
	inline bool get_win_12() const { return ___win_12; }
	inline bool* get_address_of_win_12() { return &___win_12; }
	inline void set_win_12(bool value)
	{
		___win_12 = value;
	}
};


// Hook
struct  Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Hook::mouseArrows
	int32_t ___mouseArrows_4;
	// System.Int32 Hook::rabbitArrows
	int32_t ___rabbitArrows_5;
	// System.Int32 Hook::hogArrows
	int32_t ___hogArrows_6;
	// System.Single Hook::mouseTime
	float ___mouseTime_7;
	// System.Single Hook::rabbitTime
	float ___rabbitTime_8;
	// System.Single Hook::hogTime
	float ___hogTime_9;
	// System.Boolean Hook::isDrop
	bool ___isDrop_10;
	// System.Boolean Hook::canGrab
	bool ___canGrab_11;
	// UnityEngine.Rigidbody2D Hook::main
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___main_12;
	// System.Boolean Hook::isHooked
	bool ___isHooked_13;
	// UnityEngine.GameObject Hook::Timer
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Timer_14;
	// SpawnArrows Hook::arrowsSpawn
	SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * ___arrowsSpawn_15;
	// Timer Hook::timer
	Timer_t32ADF3155A88C37628B80D227C821651666DD405 * ___timer_16;
	// Walk Hook::animal
	Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * ___animal_17;
	// System.Boolean Hook::canSet
	bool ___canSet_18;
	// Lives Hook::damage
	Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * ___damage_19;
	// UnityEngine.Animator Hook::anim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___anim_20;
	// UnityEngine.Transform Hook::startPosition
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___startPosition_21;
	// UnityEngine.Transform Hook::spawner
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___spawner_22;
	// UnityEngine.AudioSource[] Hook::sound
	AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B* ___sound_23;

public:
	inline static int32_t get_offset_of_mouseArrows_4() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___mouseArrows_4)); }
	inline int32_t get_mouseArrows_4() const { return ___mouseArrows_4; }
	inline int32_t* get_address_of_mouseArrows_4() { return &___mouseArrows_4; }
	inline void set_mouseArrows_4(int32_t value)
	{
		___mouseArrows_4 = value;
	}

	inline static int32_t get_offset_of_rabbitArrows_5() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___rabbitArrows_5)); }
	inline int32_t get_rabbitArrows_5() const { return ___rabbitArrows_5; }
	inline int32_t* get_address_of_rabbitArrows_5() { return &___rabbitArrows_5; }
	inline void set_rabbitArrows_5(int32_t value)
	{
		___rabbitArrows_5 = value;
	}

	inline static int32_t get_offset_of_hogArrows_6() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___hogArrows_6)); }
	inline int32_t get_hogArrows_6() const { return ___hogArrows_6; }
	inline int32_t* get_address_of_hogArrows_6() { return &___hogArrows_6; }
	inline void set_hogArrows_6(int32_t value)
	{
		___hogArrows_6 = value;
	}

	inline static int32_t get_offset_of_mouseTime_7() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___mouseTime_7)); }
	inline float get_mouseTime_7() const { return ___mouseTime_7; }
	inline float* get_address_of_mouseTime_7() { return &___mouseTime_7; }
	inline void set_mouseTime_7(float value)
	{
		___mouseTime_7 = value;
	}

	inline static int32_t get_offset_of_rabbitTime_8() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___rabbitTime_8)); }
	inline float get_rabbitTime_8() const { return ___rabbitTime_8; }
	inline float* get_address_of_rabbitTime_8() { return &___rabbitTime_8; }
	inline void set_rabbitTime_8(float value)
	{
		___rabbitTime_8 = value;
	}

	inline static int32_t get_offset_of_hogTime_9() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___hogTime_9)); }
	inline float get_hogTime_9() const { return ___hogTime_9; }
	inline float* get_address_of_hogTime_9() { return &___hogTime_9; }
	inline void set_hogTime_9(float value)
	{
		___hogTime_9 = value;
	}

	inline static int32_t get_offset_of_isDrop_10() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___isDrop_10)); }
	inline bool get_isDrop_10() const { return ___isDrop_10; }
	inline bool* get_address_of_isDrop_10() { return &___isDrop_10; }
	inline void set_isDrop_10(bool value)
	{
		___isDrop_10 = value;
	}

	inline static int32_t get_offset_of_canGrab_11() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___canGrab_11)); }
	inline bool get_canGrab_11() const { return ___canGrab_11; }
	inline bool* get_address_of_canGrab_11() { return &___canGrab_11; }
	inline void set_canGrab_11(bool value)
	{
		___canGrab_11 = value;
	}

	inline static int32_t get_offset_of_main_12() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___main_12)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_main_12() const { return ___main_12; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_main_12() { return &___main_12; }
	inline void set_main_12(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___main_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___main_12), (void*)value);
	}

	inline static int32_t get_offset_of_isHooked_13() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___isHooked_13)); }
	inline bool get_isHooked_13() const { return ___isHooked_13; }
	inline bool* get_address_of_isHooked_13() { return &___isHooked_13; }
	inline void set_isHooked_13(bool value)
	{
		___isHooked_13 = value;
	}

	inline static int32_t get_offset_of_Timer_14() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___Timer_14)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Timer_14() const { return ___Timer_14; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Timer_14() { return &___Timer_14; }
	inline void set_Timer_14(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Timer_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Timer_14), (void*)value);
	}

	inline static int32_t get_offset_of_arrowsSpawn_15() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___arrowsSpawn_15)); }
	inline SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * get_arrowsSpawn_15() const { return ___arrowsSpawn_15; }
	inline SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 ** get_address_of_arrowsSpawn_15() { return &___arrowsSpawn_15; }
	inline void set_arrowsSpawn_15(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * value)
	{
		___arrowsSpawn_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrowsSpawn_15), (void*)value);
	}

	inline static int32_t get_offset_of_timer_16() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___timer_16)); }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 * get_timer_16() const { return ___timer_16; }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 ** get_address_of_timer_16() { return &___timer_16; }
	inline void set_timer_16(Timer_t32ADF3155A88C37628B80D227C821651666DD405 * value)
	{
		___timer_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timer_16), (void*)value);
	}

	inline static int32_t get_offset_of_animal_17() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___animal_17)); }
	inline Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * get_animal_17() const { return ___animal_17; }
	inline Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 ** get_address_of_animal_17() { return &___animal_17; }
	inline void set_animal_17(Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * value)
	{
		___animal_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animal_17), (void*)value);
	}

	inline static int32_t get_offset_of_canSet_18() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___canSet_18)); }
	inline bool get_canSet_18() const { return ___canSet_18; }
	inline bool* get_address_of_canSet_18() { return &___canSet_18; }
	inline void set_canSet_18(bool value)
	{
		___canSet_18 = value;
	}

	inline static int32_t get_offset_of_damage_19() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___damage_19)); }
	inline Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * get_damage_19() const { return ___damage_19; }
	inline Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 ** get_address_of_damage_19() { return &___damage_19; }
	inline void set_damage_19(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * value)
	{
		___damage_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___damage_19), (void*)value);
	}

	inline static int32_t get_offset_of_anim_20() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___anim_20)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_anim_20() const { return ___anim_20; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_anim_20() { return &___anim_20; }
	inline void set_anim_20(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___anim_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_20), (void*)value);
	}

	inline static int32_t get_offset_of_startPosition_21() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___startPosition_21)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_startPosition_21() const { return ___startPosition_21; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_startPosition_21() { return &___startPosition_21; }
	inline void set_startPosition_21(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___startPosition_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startPosition_21), (void*)value);
	}

	inline static int32_t get_offset_of_spawner_22() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___spawner_22)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_spawner_22() const { return ___spawner_22; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_spawner_22() { return &___spawner_22; }
	inline void set_spawner_22(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___spawner_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawner_22), (void*)value);
	}

	inline static int32_t get_offset_of_sound_23() { return static_cast<int32_t>(offsetof(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310, ___sound_23)); }
	inline AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B* get_sound_23() const { return ___sound_23; }
	inline AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B** get_address_of_sound_23() { return &___sound_23; }
	inline void set_sound_23(AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B* value)
	{
		___sound_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sound_23), (void*)value);
	}
};


// ImageAdder
struct  ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 ImageAdder::amountOfImages
	int32_t ___amountOfImages_4;
	// System.Int32 ImageAdder::amounOfActiveImages
	int32_t ___amounOfActiveImages_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ImageAdder::images
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___images_6;

public:
	inline static int32_t get_offset_of_amountOfImages_4() { return static_cast<int32_t>(offsetof(ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281, ___amountOfImages_4)); }
	inline int32_t get_amountOfImages_4() const { return ___amountOfImages_4; }
	inline int32_t* get_address_of_amountOfImages_4() { return &___amountOfImages_4; }
	inline void set_amountOfImages_4(int32_t value)
	{
		___amountOfImages_4 = value;
	}

	inline static int32_t get_offset_of_amounOfActiveImages_5() { return static_cast<int32_t>(offsetof(ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281, ___amounOfActiveImages_5)); }
	inline int32_t get_amounOfActiveImages_5() const { return ___amounOfActiveImages_5; }
	inline int32_t* get_address_of_amounOfActiveImages_5() { return &___amounOfActiveImages_5; }
	inline void set_amounOfActiveImages_5(int32_t value)
	{
		___amounOfActiveImages_5 = value;
	}

	inline static int32_t get_offset_of_images_6() { return static_cast<int32_t>(offsetof(ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281, ___images_6)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_images_6() const { return ___images_6; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_images_6() { return &___images_6; }
	inline void set_images_6(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___images_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___images_6), (void*)value);
	}
};


// Lives
struct  Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Sprite Lives::green
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___green_4;
	// UnityEngine.Sprite Lives::red
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___red_5;
	// UnityEngine.UI.Image[] Lives::Health
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___Health_6;
	// System.Int32 Lives::damage
	int32_t ___damage_7;
	// System.Int32 Lives::Heals
	int32_t ___Heals_8;
	// UnityEngine.GameObject Lives::loseWindow
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___loseWindow_9;
	// UnityEngine.AudioSource Lives::audio
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audio_10;
	// UnityEngine.Animator Lives::anim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___anim_11;
	// System.Boolean Lives::lose
	bool ___lose_12;

public:
	inline static int32_t get_offset_of_green_4() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___green_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_green_4() const { return ___green_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_green_4() { return &___green_4; }
	inline void set_green_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___green_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___green_4), (void*)value);
	}

	inline static int32_t get_offset_of_red_5() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___red_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_red_5() const { return ___red_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_red_5() { return &___red_5; }
	inline void set_red_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___red_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___red_5), (void*)value);
	}

	inline static int32_t get_offset_of_Health_6() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___Health_6)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_Health_6() const { return ___Health_6; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_Health_6() { return &___Health_6; }
	inline void set_Health_6(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___Health_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Health_6), (void*)value);
	}

	inline static int32_t get_offset_of_damage_7() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___damage_7)); }
	inline int32_t get_damage_7() const { return ___damage_7; }
	inline int32_t* get_address_of_damage_7() { return &___damage_7; }
	inline void set_damage_7(int32_t value)
	{
		___damage_7 = value;
	}

	inline static int32_t get_offset_of_Heals_8() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___Heals_8)); }
	inline int32_t get_Heals_8() const { return ___Heals_8; }
	inline int32_t* get_address_of_Heals_8() { return &___Heals_8; }
	inline void set_Heals_8(int32_t value)
	{
		___Heals_8 = value;
	}

	inline static int32_t get_offset_of_loseWindow_9() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___loseWindow_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_loseWindow_9() const { return ___loseWindow_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_loseWindow_9() { return &___loseWindow_9; }
	inline void set_loseWindow_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___loseWindow_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loseWindow_9), (void*)value);
	}

	inline static int32_t get_offset_of_audio_10() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___audio_10)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audio_10() const { return ___audio_10; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audio_10() { return &___audio_10; }
	inline void set_audio_10(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audio_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audio_10), (void*)value);
	}

	inline static int32_t get_offset_of_anim_11() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___anim_11)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_anim_11() const { return ___anim_11; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_anim_11() { return &___anim_11; }
	inline void set_anim_11(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___anim_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_11), (void*)value);
	}

	inline static int32_t get_offset_of_lose_12() { return static_cast<int32_t>(offsetof(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727, ___lose_12)); }
	inline bool get_lose_12() const { return ___lose_12; }
	inline bool* get_address_of_lose_12() { return &___lose_12; }
	inline void set_lose_12(bool value)
	{
		___lose_12 = value;
	}
};


// ReloadScene
struct  ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String ReloadScene::main
	String_t* ___main_4;
	// System.Boolean ReloadScene::Tutor
	bool ___Tutor_5;

public:
	inline static int32_t get_offset_of_main_4() { return static_cast<int32_t>(offsetof(ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8, ___main_4)); }
	inline String_t* get_main_4() const { return ___main_4; }
	inline String_t** get_address_of_main_4() { return &___main_4; }
	inline void set_main_4(String_t* value)
	{
		___main_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___main_4), (void*)value);
	}

	inline static int32_t get_offset_of_Tutor_5() { return static_cast<int32_t>(offsetof(ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8, ___Tutor_5)); }
	inline bool get_Tutor_5() const { return ___Tutor_5; }
	inline bool* get_address_of_Tutor_5() { return &___Tutor_5; }
	inline void set_Tutor_5(bool value)
	{
		___Tutor_5 = value;
	}
};


// Rope
struct  Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.LineRenderer Rope::lr
	LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * ___lr_4;
	// UnityEngine.Transform[] Rope::points
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___points_5;

public:
	inline static int32_t get_offset_of_lr_4() { return static_cast<int32_t>(offsetof(Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8, ___lr_4)); }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * get_lr_4() const { return ___lr_4; }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 ** get_address_of_lr_4() { return &___lr_4; }
	inline void set_lr_4(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * value)
	{
		___lr_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lr_4), (void*)value);
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8, ___points_5)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_points_5() const { return ___points_5; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___points_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_5), (void*)value);
	}
};


// RopeHost
struct  RopeHost_t903B27F3928D918D402D4535C94CD5611C450872  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] RopeHost::points
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___points_4;
	// Rope RopeHost::rope
	Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * ___rope_5;

public:
	inline static int32_t get_offset_of_points_4() { return static_cast<int32_t>(offsetof(RopeHost_t903B27F3928D918D402D4535C94CD5611C450872, ___points_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_points_4() const { return ___points_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_points_4() { return &___points_4; }
	inline void set_points_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___points_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_4), (void*)value);
	}

	inline static int32_t get_offset_of_rope_5() { return static_cast<int32_t>(offsetof(RopeHost_t903B27F3928D918D402D4535C94CD5611C450872, ___rope_5)); }
	inline Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * get_rope_5() const { return ___rope_5; }
	inline Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 ** get_address_of_rope_5() { return &___rope_5; }
	inline void set_rope_5(Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * value)
	{
		___rope_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rope_5), (void*)value);
	}
};


// SpawnArrows
struct  SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 SpawnArrows::amountOfArrows
	int32_t ___amountOfArrows_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SpawnArrows::arrows
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___arrows_5;
	// System.Collections.Generic.List`1<UnityEngine.Animator> SpawnArrows::arrowsAnims
	List_1_t8726F3EB67930AF93439C3FE521CAC9E4BE0DD18 * ___arrowsAnims_6;
	// System.Int32 SpawnArrows::offset
	int32_t ___offset_7;
	// UnityEngine.GameObject SpawnArrows::arrowPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___arrowPrefab_8;
	// UnityEngine.GameObject SpawnArrows::canvs
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___canvs_9;
	// Timer SpawnArrows::timer
	Timer_t32ADF3155A88C37628B80D227C821651666DD405 * ___timer_10;
	// Lives SpawnArrows::lives
	Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * ___lives_11;
	// Hook SpawnArrows::hook
	Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * ___hook_12;
	// UnityEngine.AudioSource SpawnArrows::click
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___click_13;
	// UnityEngine.AudioSource SpawnArrows::falseClick
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___falseClick_14;

public:
	inline static int32_t get_offset_of_amountOfArrows_4() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___amountOfArrows_4)); }
	inline int32_t get_amountOfArrows_4() const { return ___amountOfArrows_4; }
	inline int32_t* get_address_of_amountOfArrows_4() { return &___amountOfArrows_4; }
	inline void set_amountOfArrows_4(int32_t value)
	{
		___amountOfArrows_4 = value;
	}

	inline static int32_t get_offset_of_arrows_5() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___arrows_5)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_arrows_5() const { return ___arrows_5; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_arrows_5() { return &___arrows_5; }
	inline void set_arrows_5(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___arrows_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrows_5), (void*)value);
	}

	inline static int32_t get_offset_of_arrowsAnims_6() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___arrowsAnims_6)); }
	inline List_1_t8726F3EB67930AF93439C3FE521CAC9E4BE0DD18 * get_arrowsAnims_6() const { return ___arrowsAnims_6; }
	inline List_1_t8726F3EB67930AF93439C3FE521CAC9E4BE0DD18 ** get_address_of_arrowsAnims_6() { return &___arrowsAnims_6; }
	inline void set_arrowsAnims_6(List_1_t8726F3EB67930AF93439C3FE521CAC9E4BE0DD18 * value)
	{
		___arrowsAnims_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrowsAnims_6), (void*)value);
	}

	inline static int32_t get_offset_of_offset_7() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___offset_7)); }
	inline int32_t get_offset_7() const { return ___offset_7; }
	inline int32_t* get_address_of_offset_7() { return &___offset_7; }
	inline void set_offset_7(int32_t value)
	{
		___offset_7 = value;
	}

	inline static int32_t get_offset_of_arrowPrefab_8() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___arrowPrefab_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_arrowPrefab_8() const { return ___arrowPrefab_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_arrowPrefab_8() { return &___arrowPrefab_8; }
	inline void set_arrowPrefab_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___arrowPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrowPrefab_8), (void*)value);
	}

	inline static int32_t get_offset_of_canvs_9() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___canvs_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_canvs_9() const { return ___canvs_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_canvs_9() { return &___canvs_9; }
	inline void set_canvs_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___canvs_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvs_9), (void*)value);
	}

	inline static int32_t get_offset_of_timer_10() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___timer_10)); }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 * get_timer_10() const { return ___timer_10; }
	inline Timer_t32ADF3155A88C37628B80D227C821651666DD405 ** get_address_of_timer_10() { return &___timer_10; }
	inline void set_timer_10(Timer_t32ADF3155A88C37628B80D227C821651666DD405 * value)
	{
		___timer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timer_10), (void*)value);
	}

	inline static int32_t get_offset_of_lives_11() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___lives_11)); }
	inline Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * get_lives_11() const { return ___lives_11; }
	inline Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 ** get_address_of_lives_11() { return &___lives_11; }
	inline void set_lives_11(Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * value)
	{
		___lives_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lives_11), (void*)value);
	}

	inline static int32_t get_offset_of_hook_12() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___hook_12)); }
	inline Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * get_hook_12() const { return ___hook_12; }
	inline Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 ** get_address_of_hook_12() { return &___hook_12; }
	inline void set_hook_12(Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * value)
	{
		___hook_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hook_12), (void*)value);
	}

	inline static int32_t get_offset_of_click_13() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___click_13)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_click_13() const { return ___click_13; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_click_13() { return &___click_13; }
	inline void set_click_13(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___click_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___click_13), (void*)value);
	}

	inline static int32_t get_offset_of_falseClick_14() { return static_cast<int32_t>(offsetof(SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8, ___falseClick_14)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_falseClick_14() const { return ___falseClick_14; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_falseClick_14() { return &___falseClick_14; }
	inline void set_falseClick_14(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___falseClick_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___falseClick_14), (void*)value);
	}
};


// Spawner
struct  Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] Spawner::Animals
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___Animals_4;
	// Walk Spawner::AnimalWalk
	Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * ___AnimalWalk_5;
	// System.Int32 Spawner::dir
	int32_t ___dir_6;
	// System.Single Spawner::Speed
	float ___Speed_7;
	// System.Single Spawner::timer
	float ___timer_8;
	// ImageAdder[] Spawner::animals
	ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* ___animals_9;
	// System.Int32 Spawner::lastChance
	int32_t ___lastChance_10;
	// System.Int32[] Spawner::chance
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___chance_11;
	// System.Collections.Generic.List`1<System.Int32> Spawner::startChance
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___startChance_12;
	// System.Int32[] Spawner::spawnTime
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___spawnTime_13;

public:
	inline static int32_t get_offset_of_Animals_4() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___Animals_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_Animals_4() const { return ___Animals_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_Animals_4() { return &___Animals_4; }
	inline void set_Animals_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___Animals_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Animals_4), (void*)value);
	}

	inline static int32_t get_offset_of_AnimalWalk_5() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___AnimalWalk_5)); }
	inline Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * get_AnimalWalk_5() const { return ___AnimalWalk_5; }
	inline Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 ** get_address_of_AnimalWalk_5() { return &___AnimalWalk_5; }
	inline void set_AnimalWalk_5(Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * value)
	{
		___AnimalWalk_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AnimalWalk_5), (void*)value);
	}

	inline static int32_t get_offset_of_dir_6() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___dir_6)); }
	inline int32_t get_dir_6() const { return ___dir_6; }
	inline int32_t* get_address_of_dir_6() { return &___dir_6; }
	inline void set_dir_6(int32_t value)
	{
		___dir_6 = value;
	}

	inline static int32_t get_offset_of_Speed_7() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___Speed_7)); }
	inline float get_Speed_7() const { return ___Speed_7; }
	inline float* get_address_of_Speed_7() { return &___Speed_7; }
	inline void set_Speed_7(float value)
	{
		___Speed_7 = value;
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___timer_8)); }
	inline float get_timer_8() const { return ___timer_8; }
	inline float* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(float value)
	{
		___timer_8 = value;
	}

	inline static int32_t get_offset_of_animals_9() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___animals_9)); }
	inline ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* get_animals_9() const { return ___animals_9; }
	inline ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81** get_address_of_animals_9() { return &___animals_9; }
	inline void set_animals_9(ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* value)
	{
		___animals_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animals_9), (void*)value);
	}

	inline static int32_t get_offset_of_lastChance_10() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___lastChance_10)); }
	inline int32_t get_lastChance_10() const { return ___lastChance_10; }
	inline int32_t* get_address_of_lastChance_10() { return &___lastChance_10; }
	inline void set_lastChance_10(int32_t value)
	{
		___lastChance_10 = value;
	}

	inline static int32_t get_offset_of_chance_11() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___chance_11)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_chance_11() const { return ___chance_11; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_chance_11() { return &___chance_11; }
	inline void set_chance_11(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___chance_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chance_11), (void*)value);
	}

	inline static int32_t get_offset_of_startChance_12() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___startChance_12)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_startChance_12() const { return ___startChance_12; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_startChance_12() { return &___startChance_12; }
	inline void set_startChance_12(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___startChance_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startChance_12), (void*)value);
	}

	inline static int32_t get_offset_of_spawnTime_13() { return static_cast<int32_t>(offsetof(Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87, ___spawnTime_13)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_spawnTime_13() const { return ___spawnTime_13; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_spawnTime_13() { return &___spawnTime_13; }
	inline void set_spawnTime_13(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___spawnTime_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawnTime_13), (void*)value);
	}
};


// Timer
struct  Timer_t32ADF3155A88C37628B80D227C821651666DD405  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image Timer::main
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___main_4;
	// System.Boolean Timer::isTime
	bool ___isTime_5;
	// System.Single Timer::time
	float ___time_6;
	// System.Single Timer::localTime
	float ___localTime_7;
	// UnityEngine.AudioSource Timer::sound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___sound_8;
	// System.Single Timer::pitching
	float ___pitching_9;

public:
	inline static int32_t get_offset_of_main_4() { return static_cast<int32_t>(offsetof(Timer_t32ADF3155A88C37628B80D227C821651666DD405, ___main_4)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_main_4() const { return ___main_4; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_main_4() { return &___main_4; }
	inline void set_main_4(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___main_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___main_4), (void*)value);
	}

	inline static int32_t get_offset_of_isTime_5() { return static_cast<int32_t>(offsetof(Timer_t32ADF3155A88C37628B80D227C821651666DD405, ___isTime_5)); }
	inline bool get_isTime_5() const { return ___isTime_5; }
	inline bool* get_address_of_isTime_5() { return &___isTime_5; }
	inline void set_isTime_5(bool value)
	{
		___isTime_5 = value;
	}

	inline static int32_t get_offset_of_time_6() { return static_cast<int32_t>(offsetof(Timer_t32ADF3155A88C37628B80D227C821651666DD405, ___time_6)); }
	inline float get_time_6() const { return ___time_6; }
	inline float* get_address_of_time_6() { return &___time_6; }
	inline void set_time_6(float value)
	{
		___time_6 = value;
	}

	inline static int32_t get_offset_of_localTime_7() { return static_cast<int32_t>(offsetof(Timer_t32ADF3155A88C37628B80D227C821651666DD405, ___localTime_7)); }
	inline float get_localTime_7() const { return ___localTime_7; }
	inline float* get_address_of_localTime_7() { return &___localTime_7; }
	inline void set_localTime_7(float value)
	{
		___localTime_7 = value;
	}

	inline static int32_t get_offset_of_sound_8() { return static_cast<int32_t>(offsetof(Timer_t32ADF3155A88C37628B80D227C821651666DD405, ___sound_8)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_sound_8() const { return ___sound_8; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_sound_8() { return &___sound_8; }
	inline void set_sound_8(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___sound_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sound_8), (void*)value);
	}

	inline static int32_t get_offset_of_pitching_9() { return static_cast<int32_t>(offsetof(Timer_t32ADF3155A88C37628B80D227C821651666DD405, ___pitching_9)); }
	inline float get_pitching_9() const { return ___pitching_9; }
	inline float* get_address_of_pitching_9() { return &___pitching_9; }
	inline void set_pitching_9(float value)
	{
		___pitching_9 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Walk
struct  Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform Walk::main
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___main_4;
	// System.Single Walk::speed
	float ___speed_5;
	// System.Int32 Walk::side
	int32_t ___side_6;
	// UnityEngine.AudioSource Walk::walk
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___walk_7;

public:
	inline static int32_t get_offset_of_main_4() { return static_cast<int32_t>(offsetof(Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029, ___main_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_main_4() const { return ___main_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_main_4() { return &___main_4; }
	inline void set_main_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___main_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___main_4), (void*)value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_side_6() { return static_cast<int32_t>(offsetof(Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029, ___side_6)); }
	inline int32_t get_side_6() const { return ___side_6; }
	inline int32_t* get_address_of_side_6() { return &___side_6; }
	inline void set_side_6(int32_t value)
	{
		___side_6 = value;
	}

	inline static int32_t get_offset_of_walk_7() { return static_cast<int32_t>(offsetof(Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029, ___walk_7)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_walk_7() const { return ___walk_7; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_walk_7() { return &___walk_7; }
	inline void set_walk_7(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___walk_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___walk_7), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ImageAdder[]
struct ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * m_Items[1];

public:
	inline ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * m_Items[1];

public:
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * m_Items[1];

public:
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * m_Items[1];

public:
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);

// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::Play(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_Play_mE5E8B1753FFDF754EAD1ACEFF6C5B6ACA506363C (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___stateName0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void ArrowStats::RandomDir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrowStats_RandomDir_m097FB8FC53250C2A2BEED7EF0B0C2CCE316E7873 (ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::RandomRange(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_RandomRange_mCB428074F55769762659348C65A7A87B88E522D6 (int32_t ___min0, int32_t ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.String UnityEngine.Component::get_tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void Counter::AnimalCollected(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Counter_AnimalCollected_m350E7A131AFE7EB7EC5619D2F854469B23C6A94D (Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Time::set_timeScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA (float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Audio.AudioMixer::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AudioMixer_SetFloat_m305579F01374620674AF66DA63DDD4BDBC9089CE (AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method);
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void ImageAdder::SetValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageAdder_SetValue_mA5A8358C7F021A158DB9C2416EB363BBDA478550 (ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * __this, int32_t ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Rigidbody2D_get_bodyType_m530E37416F48B6572E368BC92B3BA02B85F55559 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_timeScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4 (const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_bodyType_m8D34999918D42B2DF16FAAB4F237A8663EA8406B (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Walk>()
inline Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_up_mCEC23A0CF0FC3A2070C557AFD9F84F3D9991866C (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_m2360EEDAF4E9F279AAB77DBD785A7F7161865343 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___force0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void Timer::SetTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timer_SetTime_m2C010FEA2C0A8AACB10D45D4829A1AA8D2A8FC83 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, float ___setTime0, const RuntimeMethod* method);
// System.Void Hook::LaunchAnimal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook_LaunchAnimal_m4A87E70FB9E4B19A7EFF82E506A0FCFF95943ED3 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, const RuntimeMethod* method);
// System.Void Lives::ChangeHealth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Lives_ChangeHealth_mE2A807665CD8C1A4B8FAE8D11DC054164CE391E3 (Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * __this, const RuntimeMethod* method);
// System.Void Hook::AnimalSetUp(UnityEngine.Collider2D,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook_AnimalSetUp_m7197382F476EA370FA9F2BDA2F4B851B497ECA34 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___obj0, int32_t ___arrows1, float ___time2, const RuntimeMethod* method);
// System.Collections.IEnumerator Hook::SetAnimalPaternt(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Hook_SetAnimalPaternt_m2253BC223C7FD0DFF8372BBD4DDFB9EA30CC6344 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___animal0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void Hook/<SetAnimalPaternt>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetAnimalPaterntU3Ed__25__ctor_mB742D425984D061D0168939974145056BD1DC1B3 (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
inline int32_t List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
inline void List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4 (const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092 (String_t* ___sceneName0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.LineRenderer>()
inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * Component_GetComponent_TisLineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_mD5BC9EADE1AA529A5299A4D8B020FB49663DAC3A (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.LineRenderer::set_positionCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LineRenderer_set_positionCount_mA8B73AC2B9151473E94F3713E1041AA34D758AE5 (LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F (LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * __this, int32_t ___index0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, const RuntimeMethod* method);
// System.Void Rope::SetUpLine(UnityEngine.Transform[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rope_SetUpLine_mE457D644AFEA7E498BF232ED5FABB98E00F227C2 (Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * __this, TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___points0, const RuntimeMethod* method);
// System.Void SpawnArrows::SetLastArrowActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_SetLastArrowActive_m2001D319F6199B46BB817262C9FB63AD0341C510 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, const RuntimeMethod* method);
// System.Void SpawnArrows::SetAmountOfArrows(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_SetAmountOfArrows_m25A454CB577A1333F928117357B0D17FC998BB8E (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, int32_t ___amount0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<ArrowStats>()
inline ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void SpawnArrows::GetPressedButton(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_GetPressedButton_m0966981F8F7708C38832CD6550CA4267CC4C1361 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, String_t* ___key0, const RuntimeMethod* method);
// System.Void SpawnArrows::ResetArrowsValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_ResetArrowsValues_mE6914F631A10EACEC79678DEE895E8CD11C75DE3 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::get_anyKeyDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_get_anyKeyDown_m0E5B02352AC40A25BBE31F4532764279D5AF03A5 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3 (int32_t ___button0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<ArrowStats>()
inline ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * Component_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_mDB796D00857A64249229F6FC894C95FB0C030A2A (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared)(__this, ___item0, method);
}
// System.Void Spawner::ResetChances()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_ResetChances_mA8C792419BBF97274D2E75B89F8B3D65E6F6C8AD (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, const RuntimeMethod* method);
// System.Void Spawner::SpawnAnimal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_SpawnAnimal_mE780A4641654B1587610AF4A62A070E4BB681163 (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator Spawner::WaitAndPrint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Spawner_WaitAndPrint_m84781AD3E91DCCE5F55370470FEB6EDAAD7A90D6 (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, float ___waitTime0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline)(__this, ___index0, method);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared)(___original0, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Walk>()
inline Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * GameObject_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mDCE712F19A37A792449BA93CB0736F41B4EE17E7 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void Spawner/<WaitAndPrint>d__14::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitAndPrintU3Ed__14__ctor_m13628828E6174F666C9D54AF360AA61A53B8FF06 (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared)(__this, method);
}
// System.Collections.IEnumerator Timer::ChangePitch(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Timer_ChangePitch_m27DC73EAF0836A0D0ED3567498BAC3850E8C5ED5 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, float ___waitTime0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Pause_mC4F9932A77B6AA2CC3FB720721B7837CF57B675D (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_pitch_m9E9EEFADFAB44ADEC656573425CF34D0069A0CBC (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopCoroutine_m4DB2A899F9BDF8CA3264DD8C4130E767702B626B (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, String_t* ___methodName0, const RuntimeMethod* method);
// System.Collections.IEnumerator Timer::TimerStart(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Timer_TimerStart_m2A9D651E9D42FA173C24B884EB81F3D181D6D558 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, float ___waitTime0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, float ___value0, const RuntimeMethod* method);
// System.Void Timer/<ChangePitch>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangePitchU3Ed__11__ctor_m2801AE84732F6D9E51B98C1C637A4C32B9652F5D (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void Timer/<TimerStart>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTimerStartU3Ed__12__ctor_m1C21FE74F4545C01E22A3A35B87ABDD93BB0E1FB (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Transform>()
inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.AudioSource::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ArrowStats::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrowStats_OnEnable_m85CA157B9B0ECEC641DB4F28506FE456DAF562B9 (ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2BB13E85B9058564A98D5618BDD8518B142BD05E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// transform.GetComponent<Animator>().Play("DoNothing");
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1;
		L_1 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(L_0, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		Animator_Play_mE5E8B1753FFDF754EAD1ACEFF6C5B6ACA506363C(L_1, _stringLiteral2BB13E85B9058564A98D5618BDD8518B142BD05E, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ArrowStats::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrowStats_Start_m9306E8DEB76F28D798461E844AEBD8FA79BBAD99 (ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// background = transform.GetComponent<Image>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_1;
		L_1 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB(L_0, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		__this->set_background_6(L_1);
		// RandomDir();
		ArrowStats_RandomDir_m097FB8FC53250C2A2BEED7EF0B0C2CCE316E7873(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ArrowStats::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrowStats_Update_mF97643EAE181D86207D4055B549EAA532F08F23C (ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * __this, const RuntimeMethod* method)
{
	{
		// if (IsActive == true)
		bool L_0 = __this->get_IsActive_10();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		// ActiveBackgroundSprite.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_ActiveBackgroundSprite_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// backgroundSprite.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_backgroundSprite_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0021:
	{
		// ActiveBackgroundSprite.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_ActiveBackgroundSprite_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// backgroundSprite.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_backgroundSprite_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ArrowStats::RandomDir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrowStats_RandomDir_m097FB8FC53250C2A2BEED7EF0B0C2CCE316E7873 (ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// for (int i = 0; i < arrowImg.Length; i++)
		V_1 = 0;
		goto IL_0016;
	}

IL_0004:
	{
		// arrowImg[i].SetActive(false);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_arrowImg_5();
		int32_t L_1 = V_1;
		int32_t L_2 = L_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// for (int i = 0; i < arrowImg.Length; i++)
		int32_t L_4 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0016:
	{
		// for (int i = 0; i < arrowImg.Length; i++)
		int32_t L_5 = V_1;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_6 = __this->get_arrowImg_5();
		if ((((int32_t)L_5) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// int rand = Random.RandomRange(0, 4);
		int32_t L_7;
		L_7 = Random_RandomRange_mCB428074F55769762659348C65A7A87B88E522D6(0, 4, /*hidden argument*/NULL);
		V_0 = L_7;
		// ArrowDir = arrowDirs[rand];
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = __this->get_arrowDirs_4();
		int32_t L_9 = V_0;
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		__this->set_ArrowDir_9(L_11);
		// arrowImg[rand].SetActive(true);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_12 = __this->get_arrowImg_5();
		int32_t L_13 = V_0;
		int32_t L_14 = L_13;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = (L_12)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_14));
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_15, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ArrowStats::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrowStats__ctor_m44A6A13BE4C29C7C576FD54B4E9BAD7BFDF7D9FE (ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private string[] arrowDirs=new string[] {"right","left","up","down" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		ArrayElementTypeCheck (L_1, _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		ArrayElementTypeCheck (L_2, _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		ArrayElementTypeCheck (L_3, _stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		ArrayElementTypeCheck (L_4, _stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		__this->set_arrowDirs_4(L_4);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Border::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Border_OnTriggerEnter2D_mF00A6CA6D75E8C330ECDDD95389E1125C9529256 (Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral294F098A1D5E681EDEE8F43BFA8F45CFBA5798A7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral31540315C86289571C1A0ECD739DD7089EB97113);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9984A7A08D209967ED7C21E876406F398FD430C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC9A396D4E20D45B3E5DA31508C481845429502A5);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// switch (collision.tag)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collision0;
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_00f2;
		}
	}
	{
		String_t* L_3 = V_0;
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, _stringLiteral9984A7A08D209967ED7C21E876406F398FD430C7, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_5 = V_0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, _stringLiteral294F098A1D5E681EDEE8F43BFA8F45CFBA5798A7, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_7 = V_0;
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_7, _stringLiteral31540315C86289571C1A0ECD739DD7089EB97113, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0087;
		}
	}
	{
		String_t* L_9 = V_0;
		bool L_10;
		L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_9, _stringLiteralC9A396D4E20D45B3E5DA31508C481845429502A5, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00bd;
		}
	}
	{
		return;
	}

IL_0042:
	{
		// Destroy(collision.gameObject);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_11 = ___collision0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12;
		L_12 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_12, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_004e:
	{
		// Destroy(collision.gameObject);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_13 = ___collision0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_14, /*hidden argument*/NULL);
		// if (Ceiling)
		bool L_15 = __this->get_Ceiling_4();
		if (!L_15)
		{
			goto IL_00f2;
		}
	}
	{
		// Plus.AnimalCollected(0);
		Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * L_16 = __this->get_Plus_5();
		Counter_AnimalCollected_m350E7A131AFE7EB7EC5619D2F854469B23C6A94D(L_16, 0, /*hidden argument*/NULL);
		// particle.Play();
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_17 = __this->get_particle_6();
		ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50(L_17, /*hidden argument*/NULL);
		// audiox.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_18 = __this->get_audiox_7();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_18, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0087:
	{
		// Destroy(collision.gameObject);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_19 = ___collision0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20;
		L_20 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_20, /*hidden argument*/NULL);
		// if (Ceiling)
		bool L_21 = __this->get_Ceiling_4();
		if (!L_21)
		{
			goto IL_00f2;
		}
	}
	{
		// Plus.AnimalCollected(1);
		Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * L_22 = __this->get_Plus_5();
		Counter_AnimalCollected_m350E7A131AFE7EB7EC5619D2F854469B23C6A94D(L_22, 1, /*hidden argument*/NULL);
		// particle.Play();
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_23 = __this->get_particle_6();
		ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50(L_23, /*hidden argument*/NULL);
		// audiox.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_24 = __this->get_audiox_7();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_24, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00bd:
	{
		// Destroy(collision.gameObject);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_25 = ___collision0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26;
		L_26 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_26, /*hidden argument*/NULL);
		// if (Ceiling)
		bool L_27 = __this->get_Ceiling_4();
		if (!L_27)
		{
			goto IL_00f2;
		}
	}
	{
		// Plus.AnimalCollected(2);
		Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * L_28 = __this->get_Plus_5();
		Counter_AnimalCollected_m350E7A131AFE7EB7EC5619D2F854469B23C6A94D(L_28, 2, /*hidden argument*/NULL);
		// particle.Play();
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_29 = __this->get_particle_6();
		ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50(L_29, /*hidden argument*/NULL);
		// audiox.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_30 = __this->get_audiox_7();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_30, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		// }
		return;
	}
}
// System.Void Border::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Border__ctor_mC47B2974B694148D0553A8B1BA49C1C06A4CB974 (Border_t876CADC0FC798ACEC33A5C753E2997897E4794D6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChangeMenu::Change()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeMenu_Change_mB9D17789278243192BDDE3F04EFBCFBC1DD4A5B3 (ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641 * __this, const RuntimeMethod* method)
{
	{
		// changer = !changer;
		bool L_0 = __this->get_changer_4();
		__this->set_changer_4((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// if (changer)
		bool L_1 = __this->get_changer_4();
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		// Menu.SetActive(!changer);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_Menu_5();
		bool L_3 = __this->get_changer_4();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// Settings.SetActive(changer);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_Settings_6();
		bool L_5 = __this->get_changer_4();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}

IL_003d:
	{
		// Menu.SetActive(!changer);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_Menu_5();
		bool L_7 = __this->get_changer_4();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// Settings.SetActive(changer);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_Settings_6();
		bool L_9 = __this->get_changer_4();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_8, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ChangeMenu::ClearTutor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeMenu_ClearTutor_m51B62CD977BA017AB8DE25E3033B6C2A27C51A51 (ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641 * __this, const RuntimeMethod* method)
{
	{
		// Settings.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_Settings_6();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// Time.timeScale = 1;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ChangeMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeMenu__ctor_m8590CABB0EE49A4F89D6AE45EAABA1929A2C85AF (ChangeMenu_t96F3EC43C62B144389A9925C250CF389CC9F5641 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChangeVolume::SetSound(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeVolume_SetSound_m7C9168882B9895E62C1018BCE3CD15EC74721193 (ChangeVolume_t4B1572DD63F4E337091284BB3DA30C4EFC21A0A1 * __this, float ___set0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3C34217CBB8F650ED8F6E70B410A604371E2EF1);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float value = Mathf.Lerp(-60, 20, set);
		float L_0 = ___set0;
		float L_1;
		L_1 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616((-60.0f), (20.0f), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// mixer.SetFloat("Sound",value);
		AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * L_2 = __this->get_mixer_4();
		float L_3 = V_0;
		bool L_4;
		L_4 = AudioMixer_SetFloat_m305579F01374620674AF66DA63DDD4BDBC9089CE(L_2, _stringLiteralB3C34217CBB8F650ED8F6E70B410A604371E2EF1, L_3, /*hidden argument*/NULL);
		// if (value == -60)
		float L_5 = V_0;
		if ((!(((float)L_5) == ((float)(-60.0f)))))
		{
			goto IL_0031;
		}
	}
	{
		// value = -80;
		V_0 = (-80.0f);
	}

IL_0031:
	{
		// }
		return;
	}
}
// System.Void ChangeVolume::SetMusic(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeVolume_SetMusic_m0D15100E9B1178AAC34CD8C7D4969605E36527BF (ChangeVolume_t4B1572DD63F4E337091284BB3DA30C4EFC21A0A1 * __this, float ___set0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D6BA8D36FA648F5F7EC00E75E4E3CB92589DA2E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// float value = Mathf.Lerp(-60, 20, set);
		float L_0 = ___set0;
		float L_1;
		L_1 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616((-60.0f), (20.0f), L_0, /*hidden argument*/NULL);
		// mixer.SetFloat("Music", Mathf.Lerp(-60, 20, set));
		AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * L_2 = __this->get_mixer_4();
		float L_3 = ___set0;
		float L_4;
		L_4 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616((-60.0f), (20.0f), L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = AudioMixer_SetFloat_m305579F01374620674AF66DA63DDD4BDBC9089CE(L_2, _stringLiteral9D6BA8D36FA648F5F7EC00E75E4E3CB92589DA2E, L_4, /*hidden argument*/NULL);
		// if (value == -60)
		// }
		return;
	}
}
// System.Void ChangeVolume::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeVolume__ctor_mEF39DA9624C132587BC057E45EDA63FFB21968F6 (ChangeVolume_t4B1572DD63F4E337091284BB3DA30C4EFC21A0A1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Counter::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Counter_Start_m77A2B23760AA7DA12C9BBEE742221AC01B67AFB3 (Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < points.Length; i++)
		V_0 = 0;
		goto IL_001d;
	}

IL_0004:
	{
		// points[i] = count[i].amountOfImages;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = __this->get_points_6();
		int32_t L_1 = V_0;
		ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* L_2 = __this->get_count_5();
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		int32_t L_6 = L_5->get_amountOfImages_4();
		(L_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_1), (int32_t)L_6);
		// for (int i = 0; i < points.Length; i++)
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_001d:
	{
		// for (int i = 0; i < points.Length; i++)
		int32_t L_8 = V_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_9 = __this->get_points_6();
		if ((((int32_t)L_8) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Counter::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Counter_Update_m750B99E43903C32501752EDB091B76D4D8591063 (Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8E4F39EF3084FF922002C13AFD0E988627D2CA29);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (AnimalsCollected == AnimalsNeed)
		int32_t L_0 = __this->get_AnimalsCollected_8();
		int32_t L_1 = __this->get_AnimalsNeed_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_006d;
		}
	}
	{
		// winWindow.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_winWindow_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)1, /*hidden argument*/NULL);
		// if (win == false)
		bool L_3 = __this->get_win_12();
		if (L_3)
		{
			goto IL_0045;
		}
	}
	{
		// anim.SetBool("Change", true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4 = __this->get_anim_11();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_4, _stringLiteral8E4F39EF3084FF922002C13AFD0E988627D2CA29, (bool)1, /*hidden argument*/NULL);
		// audio.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_5 = __this->get_audio_10();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_5, /*hidden argument*/NULL);
		// win = true;
		__this->set_win_12((bool)1);
	}

IL_0045:
	{
		// if (!audio.isPlaying)
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_6 = __this->get_audio_10();
		bool L_7;
		L_7 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_006d;
		}
	}
	{
		// Time.timeScale = 0;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// anim.SetBool("Change", false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_8 = __this->get_anim_11();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_8, _stringLiteral8E4F39EF3084FF922002C13AFD0E988627D2CA29, (bool)0, /*hidden argument*/NULL);
	}

IL_006d:
	{
		// }
		return;
	}
}
// System.Void Counter::AnimalCollected(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Counter_AnimalCollected_m350E7A131AFE7EB7EC5619D2F854469B23C6A94D (Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// points[value]--;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = __this->get_points_6();
		int32_t L_1 = ___value0;
		int32_t* L_2 = ((L_0)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_1)));
		int32_t L_3 = *((int32_t*)L_2);
		*((int32_t*)L_2) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1));
		// count[value].SetValue(points[value]);
		ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* L_4 = __this->get_count_5();
		int32_t L_5 = ___value0;
		int32_t L_6 = L_5;
		ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_8 = __this->get_points_6();
		int32_t L_9 = ___value0;
		int32_t L_10 = L_9;
		int32_t L_11 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		ImageAdder_SetValue_mA5A8358C7F021A158DB9C2416EB363BBDA478550(L_7, L_11, /*hidden argument*/NULL);
		// if (points[value] == 0)
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_12 = __this->get_points_6();
		int32_t L_13 = ___value0;
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_14));
		if (L_15)
		{
			goto IL_003e;
		}
	}
	{
		// AnimalsCollected++;
		int32_t L_16 = __this->get_AnimalsCollected_8();
		__this->set_AnimalsCollected_8(((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1)));
	}

IL_003e:
	{
		// }
		return;
	}
}
// System.Void Counter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Counter__ctor_m12F27AA8C9EDE176E9543076DB214639246AAC73 (Counter_t6A281326FFA8D37BB4BCEBAD26FC34A124B9A24D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private int[] points = new int[3];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		__this->set_points_6(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Hook::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook_Start_m24B908B8FD7C19B750AC841EE5FAE4A36339A4F1 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// main = gameObject.GetComponent<Rigidbody2D>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_1;
		L_1 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		__this->set_main_12(L_1);
		// anim = transform.GetComponent<Animator>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3;
		L_3 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(L_2, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set_anim_20(L_3);
		// }
		return;
	}
}
// System.Void Hook::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook_Update_m754647819A7215B7620FF44950F52962DB13FD76 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * V_0 = NULL;
	{
		// if (Input.GetKeyDown(KeyCode.Space)&& main.bodyType == RigidbodyType2D.Static&&Time.timeScale!=0)
		bool L_0;
		L_0 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_1 = __this->get_main_12();
		int32_t L_2;
		L_2 = Rigidbody2D_get_bodyType_m530E37416F48B6572E368BC92B3BA02B85F55559(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_0037;
		}
	}
	{
		float L_3;
		L_3 = Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4(/*hidden argument*/NULL);
		if ((((float)L_3) == ((float)(0.0f))))
		{
			goto IL_0037;
		}
	}
	{
		// isDrop = true;
		__this->set_isDrop_10((bool)1);
		// sound[0].Play();
		AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B* L_4 = __this->get_sound_23();
		int32_t L_5 = 0;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_6 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_6, /*hidden argument*/NULL);
	}

IL_0037:
	{
		// if (isDrop)
		bool L_7 = __this->get_isDrop_10();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		// main.bodyType = RigidbodyType2D.Dynamic;
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_8 = __this->get_main_12();
		Rigidbody2D_set_bodyType_m8D34999918D42B2DF16FAAB4F237A8663EA8406B(L_8, 0, /*hidden argument*/NULL);
		// }else
		goto IL_0059;
	}

IL_004d:
	{
		// main.bodyType = RigidbodyType2D.Static;
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_9 = __this->get_main_12();
		Rigidbody2D_set_bodyType_m8D34999918D42B2DF16FAAB4F237A8663EA8406B(L_9, 2, /*hidden argument*/NULL);
	}

IL_0059:
	{
		// if (canGrab == false&&transform.childCount>2)
		bool L_10 = __this->get_canGrab_11();
		if (L_10)
		{
			goto IL_0108;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		int32_t L_12;
		L_12 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) <= ((int32_t)2)))
		{
			goto IL_0108;
		}
	}
	{
		// Walk Animal = transform.GetChild(transform.childCount-1).transform.GetComponent<Walk>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		int32_t L_15;
		L_15 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_14, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_13, ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)1)), /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_16, /*hidden argument*/NULL);
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_18;
		L_18 = Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF(L_17, /*hidden argument*/Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF_RuntimeMethod_var);
		V_0 = L_18;
		// transform.GetChild(transform.childCount-1).transform.parent = null;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		int32_t L_21;
		L_21 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_20, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_19, ((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)1)), /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23;
		L_23 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_22, /*hidden argument*/NULL);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_23, (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)NULL, /*hidden argument*/NULL);
		// Animal.speed = 1f;
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_24 = V_0;
		L_24->set_speed_5((1.0f));
		// Animal.transform.position = new Vector3(Animal.transform.position.x, spawner.position.y, 0);
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_25 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26;
		L_26 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_25, /*hidden argument*/NULL);
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_27 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_27, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_28, /*hidden argument*/NULL);
		float L_30 = L_29.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31 = __this->get_spawner_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_31, /*hidden argument*/NULL);
		float L_33 = L_32.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_34), L_30, L_33, (0.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_26, L_34, /*hidden argument*/NULL);
		// sound[1].Play();
		AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B* L_35 = __this->get_sound_23();
		int32_t L_36 = 1;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_37 = (L_35)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_36));
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_37, /*hidden argument*/NULL);
	}

IL_0108:
	{
		// }
		return;
	}
}
// System.Void Hook::LaunchAnimal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook_LaunchAnimal_m4A87E70FB9E4B19A7EFF82E506A0FCFF95943ED3 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7D832450EB8F529B98D1D7829EE23B628D4FD794);
		s_Il2CppMethodInitialized = true;
	}
	{
		// main.AddForce(Vector2.up * 40, ForceMode2D.Impulse);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_0 = __this->get_main_12();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = Vector2_get_up_mCEC23A0CF0FC3A2070C557AFD9F84F3D9991866C(/*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		L_2 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_1, (40.0f), /*hidden argument*/NULL);
		Rigidbody2D_AddForce_m2360EEDAF4E9F279AAB77DBD785A7F7161865343(L_0, L_2, 1, /*hidden argument*/NULL);
		// anim.SetBool("Hold", false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3 = __this->get_anim_20();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_3, _stringLiteral7D832450EB8F529B98D1D7829EE23B628D4FD794, (bool)0, /*hidden argument*/NULL);
		// sound[2].Play();
		AudioSourceU5BU5D_t29E81D0D3B6FB9B7E7DDDBDDA32E38026AA4D12B* L_4 = __this->get_sound_23();
		int32_t L_5 = 2;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_6 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Hook::AnimalSetUp(UnityEngine.Collider2D,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook_AnimalSetUp_m7197382F476EA370FA9F2BDA2F4B851B497ECA34 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___obj0, int32_t ___arrows1, float ___time2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7D832450EB8F529B98D1D7829EE23B628D4FD794);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animal = obj.transform.GetComponent<Walk>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___obj0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_2;
		L_2 = Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF(L_1, /*hidden argument*/Component_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mE6DA2268B3EC3F54266FD18327B9A79EFCEB5DBF_RuntimeMethod_var);
		__this->set_animal_17(L_2);
		// animal.speed = 0;
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_3 = __this->get_animal_17();
		L_3->set_speed_5((0.0f));
		// anim.SetBool("Hold", true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4 = __this->get_anim_20();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_4, _stringLiteral7D832450EB8F529B98D1D7829EE23B628D4FD794, (bool)1, /*hidden argument*/NULL);
		// isHooked = true;
		__this->set_isHooked_13((bool)1);
		// canGrab = true;
		__this->set_canGrab_11((bool)1);
		// Timer.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_Timer_14();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// arrowsSpawn.amountOfArrows = arrows;
		SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * L_6 = __this->get_arrowsSpawn_15();
		int32_t L_7 = ___arrows1;
		L_6->set_amountOfArrows_4(L_7);
		// timer.SetTime(time);
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_8 = __this->get_timer_16();
		float L_9 = ___time2;
		Timer_SetTime_m2C010FEA2C0A8AACB10D45D4829A1AA8D2A8FC83(L_8, L_9, /*hidden argument*/NULL);
		// canSet = true;
		__this->set_canSet_18((bool)1);
		// }
		return;
	}
}
// System.Void Hook::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook_OnTriggerEnter2D_m29375DE9BB6C0820DDA4B0AA450BAEDC4DFE4347 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01DABAEA1038CE94C68EC1934C99F3FAE1140A8B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral294F098A1D5E681EDEE8F43BFA8F45CFBA5798A7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral31540315C86289571C1A0ECD739DD7089EB97113);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9984A7A08D209967ED7C21E876406F398FD430C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC9A396D4E20D45B3E5DA31508C481845429502A5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Hedgehog" && isHooked == false)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteral9984A7A08D209967ED7C21E876406F398FD430C7, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		bool L_3 = __this->get_isHooked_13();
		if (L_3)
		{
			goto IL_002b;
		}
	}
	{
		// LaunchAnimal();
		Hook_LaunchAnimal_m4A87E70FB9E4B19A7EFF82E506A0FCFF95943ED3(__this, /*hidden argument*/NULL);
		// damage.ChangeHealth();
		Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * L_4 = __this->get_damage_19();
		Lives_ChangeHealth_mE2A807665CD8C1A4B8FAE8D11DC054164CE391E3(L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		// if (other.tag == "Mouse" && isHooked == false)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_5 = ___other0;
		String_t* L_6;
		L_6 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_5, /*hidden argument*/NULL);
		bool L_7;
		L_7 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, _stringLiteral294F098A1D5E681EDEE8F43BFA8F45CFBA5798A7, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		bool L_8 = __this->get_isHooked_13();
		if (L_8)
		{
			goto IL_0066;
		}
	}
	{
		// AnimalSetUp(other, mouseArrows, mouseTime);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_9 = ___other0;
		int32_t L_10 = __this->get_mouseArrows_4();
		float L_11 = __this->get_mouseTime_7();
		Hook_AnimalSetUp_m7197382F476EA370FA9F2BDA2F4B851B497ECA34(__this, L_9, L_10, L_11, /*hidden argument*/NULL);
		// StartCoroutine(SetAnimalPaternt(other));
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_12 = ___other0;
		RuntimeObject* L_13;
		L_13 = Hook_SetAnimalPaternt_m2253BC223C7FD0DFF8372BBD4DDFB9EA30CC6344(__this, L_12, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_14;
		L_14 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_13, /*hidden argument*/NULL);
	}

IL_0066:
	{
		// if (other.tag == "Rabbit" && isHooked == false)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_15 = ___other0;
		String_t* L_16;
		L_16 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_15, /*hidden argument*/NULL);
		bool L_17;
		L_17 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_16, _stringLiteral31540315C86289571C1A0ECD739DD7089EB97113, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a1;
		}
	}
	{
		bool L_18 = __this->get_isHooked_13();
		if (L_18)
		{
			goto IL_00a1;
		}
	}
	{
		// AnimalSetUp(other, rabbitArrows, rabbitTime);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_19 = ___other0;
		int32_t L_20 = __this->get_rabbitArrows_5();
		float L_21 = __this->get_rabbitTime_8();
		Hook_AnimalSetUp_m7197382F476EA370FA9F2BDA2F4B851B497ECA34(__this, L_19, L_20, L_21, /*hidden argument*/NULL);
		// StartCoroutine(SetAnimalPaternt(other));
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_22 = ___other0;
		RuntimeObject* L_23;
		L_23 = Hook_SetAnimalPaternt_m2253BC223C7FD0DFF8372BBD4DDFB9EA30CC6344(__this, L_22, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_24;
		L_24 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_23, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		// if (other.tag == "Hog" && isHooked == false)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_25 = ___other0;
		String_t* L_26;
		L_26 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_25, /*hidden argument*/NULL);
		bool L_27;
		L_27 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_26, _stringLiteralC9A396D4E20D45B3E5DA31508C481845429502A5, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00dc;
		}
	}
	{
		bool L_28 = __this->get_isHooked_13();
		if (L_28)
		{
			goto IL_00dc;
		}
	}
	{
		// AnimalSetUp(other,hogArrows, hogTime);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_29 = ___other0;
		int32_t L_30 = __this->get_hogArrows_6();
		float L_31 = __this->get_hogTime_9();
		Hook_AnimalSetUp_m7197382F476EA370FA9F2BDA2F4B851B497ECA34(__this, L_29, L_30, L_31, /*hidden argument*/NULL);
		// StartCoroutine(SetAnimalPaternt(other));
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_32 = ___other0;
		RuntimeObject* L_33;
		L_33 = Hook_SetAnimalPaternt_m2253BC223C7FD0DFF8372BBD4DDFB9EA30CC6344(__this, L_32, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_34;
		L_34 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_33, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		// if(other.tag == "Ceiling")
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_35 = ___other0;
		String_t* L_36;
		L_36 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_35, /*hidden argument*/NULL);
		bool L_37;
		L_37 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_36, _stringLiteral01DABAEA1038CE94C68EC1934C99F3FAE1140A8B, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_012e;
		}
	}
	{
		// main.velocity = Vector3.zero;
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_38 = __this->get_main_12();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39;
		L_39 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_40;
		L_40 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_39, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_38, L_40, /*hidden argument*/NULL);
		// transform.position = startPosition.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_41;
		L_41 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_42 = __this->get_startPosition_21();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
		L_43 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_42, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_41, L_43, /*hidden argument*/NULL);
		// canGrab = true;
		__this->set_canGrab_11((bool)1);
		// isDrop = false;
		__this->set_isDrop_10((bool)0);
		// isHooked = false;
		__this->set_isHooked_13((bool)0);
	}

IL_012e:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator Hook::SetAnimalPaternt(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Hook_SetAnimalPaternt_m2253BC223C7FD0DFF8372BBD4DDFB9EA30CC6344 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___animal0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * L_0 = (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 *)il2cpp_codegen_object_new(U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835_il2cpp_TypeInfo_var);
		U3CSetAnimalPaterntU3Ed__25__ctor_mB742D425984D061D0168939974145056BD1DC1B3(L_0, 0, /*hidden argument*/NULL);
		U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * L_1 = L_0;
		L_1->set_U3CU3E4__this_3(__this);
		U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * L_2 = L_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_3 = ___animal0;
		L_2->set_animal_2(L_3);
		return L_2;
	}
}
// System.Void Hook::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hook__ctor_m693B9FDDE2C931A45DDC1B9A1C28EB7915678254 (Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * __this, const RuntimeMethod* method)
{
	{
		// bool canSet = true;
		__this->set_canSet_18((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImageAdder::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageAdder_Awake_m9E8C22521946B3CCA47744E7F7C40900BE67FA1D (ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * __this, const RuntimeMethod* method)
{
	{
		// amounOfActiveImages = amountOfImages;
		int32_t L_0 = __this->get_amountOfImages_4();
		__this->set_amounOfActiveImages_5(L_0);
		// }
		return;
	}
}
// System.Void ImageAdder::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageAdder_Start_m458792536E8897B7C4E56278B5EC120A28E89BE4 (ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * __this, const RuntimeMethod* method)
{
	{
		// SetValue(amountOfImages);
		int32_t L_0 = __this->get_amountOfImages_4();
		ImageAdder_SetValue_mA5A8358C7F021A158DB9C2416EB363BBDA478550(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ImageAdder::SetValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageAdder_SetValue_mA5A8358C7F021A158DB9C2416EB363BBDA478550 (ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (value >= 0)
		int32_t L_0 = ___value0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		// for (int i = 0; i < images.Count; i++)
		V_0 = 0;
		goto IL_0038;
	}

IL_0008:
	{
		// if (value - 1 >= i)
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1))) < ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		// images[i].SetActive(true);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_3 = __this->get_images_6();
		int32_t L_4 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_3, L_4, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		goto IL_0034;
	}

IL_0022:
	{
		// images[i].SetActive(false);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_6 = __this->get_images_6();
		int32_t L_7 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8;
		L_8 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_8, (bool)0, /*hidden argument*/NULL);
	}

IL_0034:
	{
		// for (int i = 0; i < images.Count; i++)
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0038:
	{
		// for (int i = 0; i < images.Count; i++)
		int32_t L_10 = V_0;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_11 = __this->get_images_6();
		int32_t L_12;
		L_12 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_11, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0008;
		}
	}
	{
		// if(value>1)
		int32_t L_13 = ___value0;
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0051;
		}
	}
	{
		// amounOfActiveImages = value;
		int32_t L_14 = ___value0;
		__this->set_amounOfActiveImages_5(L_14);
	}

IL_0051:
	{
		// }
		return;
	}
}
// System.Void ImageAdder::RemoveImage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageAdder_RemoveImage_mDAE0E516DA388FB5D4EFE3C8DA48194C8A5AF7DA (ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * __this, int32_t ___imageValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1051ECAD376DFA7401D0FAA334972924BD291944);
		s_Il2CppMethodInitialized = true;
	}
	{
		// images[imageValue].transform.GetComponent<Animator>().SetBool("Remove",true);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = __this->get_images_6();
		int32_t L_1 = ___imageValue0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_2, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4;
		L_4 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(L_3, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_4, _stringLiteral1051ECAD376DFA7401D0FAA334972924BD291944, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ImageAdder::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageAdder_Update_m35A08A2C257A766E998BDF05BCFC7F570CF1987F (ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ImageAdder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImageAdder__ctor_m6709E21B8CDF0D20E75DC9E46607ABDC6A4E3B4A (ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private List<GameObject> images = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_0, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_images_6(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lives::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Lives_Update_m4FF28CBFFB9B3F0A5E85BF9308691C8682AFF75D (Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8E4F39EF3084FF922002C13AFD0E988627D2CA29);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// Heals = Health.Length - damage;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_0 = __this->get_Health_6();
		int32_t L_1 = __this->get_damage_7();
		__this->set_Heals_8(((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))), (int32_t)L_1)));
		// if (Heals >= 0 && Heals < Health.Length)
		int32_t L_2 = __this->get_Heals_8();
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_3 = __this->get_Heals_8();
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_4 = __this->get_Health_6();
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0059;
		}
	}
	{
		// for (int i = 0; i < Health.Length; i++)
		V_0 = 0;
		goto IL_004e;
	}

IL_0032:
	{
		// Health[Heals].sprite = red;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_5 = __this->get_Health_6();
		int32_t L_6 = __this->get_Heals_8();
		int32_t L_7 = L_6;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_8 = (L_5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7));
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_9 = __this->get_red_5();
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_8, L_9, /*hidden argument*/NULL);
		// for (int i = 0; i < Health.Length; i++)
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_004e:
	{
		// for (int i = 0; i < Health.Length; i++)
		int32_t L_11 = V_0;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_12 = __this->get_Health_6();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))
		{
			goto IL_0032;
		}
	}

IL_0059:
	{
		// if (Heals <= 0)
		int32_t L_13 = __this->get_Heals_8();
		if ((((int32_t)L_13) > ((int32_t)0)))
		{
			goto IL_00c1;
		}
	}
	{
		// loseWindow.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = __this->get_loseWindow_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_14, (bool)1, /*hidden argument*/NULL);
		// if (lose == false)
		bool L_15 = __this->get_lose_12();
		if (L_15)
		{
			goto IL_0099;
		}
	}
	{
		// anim.SetBool("Change", true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_16 = __this->get_anim_11();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_16, _stringLiteral8E4F39EF3084FF922002C13AFD0E988627D2CA29, (bool)1, /*hidden argument*/NULL);
		// audio.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_17 = __this->get_audio_10();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_17, /*hidden argument*/NULL);
		// lose = true;
		__this->set_lose_12((bool)1);
	}

IL_0099:
	{
		// if (!audio.isPlaying)
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_18 = __this->get_audio_10();
		bool L_19;
		L_19 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00c1;
		}
	}
	{
		// Time.timeScale = 0;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// anim.SetBool("Change", false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_20 = __this->get_anim_11();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_20, _stringLiteral8E4F39EF3084FF922002C13AFD0E988627D2CA29, (bool)0, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		// }
		return;
	}
}
// System.Void Lives::ChangeHealth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Lives_ChangeHealth_mE2A807665CD8C1A4B8FAE8D11DC054164CE391E3 (Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * __this, const RuntimeMethod* method)
{
	{
		// damage++;
		int32_t L_0 = __this->get_damage_7();
		__this->set_damage_7(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		// }
		return;
	}
}
// System.Void Lives::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Lives__ctor_m77E2BF995DBECEBFB9B04F92EE9C0863AC438AEC (Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ReloadScene::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReloadScene_Start_mC5ABE315E40286FE314F4F60294A4FA5A5EEE540 (ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// main = SceneManager.GetActiveScene().name;
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_0;
		L_0 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1;
		L_1 = Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_0), /*hidden argument*/NULL);
		__this->set_main_4(L_1);
		// if (Tutor)
		bool L_2 = __this->get_Tutor_5();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		// Time.timeScale = 0;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// }
		return;
	}

IL_0026:
	{
		// Time.timeScale = 1;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ReloadScene::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReloadScene_Update_mB265B9700FC2A03B8C5C3D496FF6FC3B070E16D2 (ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ReloadScene::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReloadScene_LoadScene_mB265DE58BB90E668D1369936550B4439184E540B (ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(value);
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ReloadScene::ReLoad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReloadScene_ReLoad_mC5D67DAECB09026B8AAE83DAF21510F6128DE45E (ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(main);
		String_t* L_0 = __this->get_main_4();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ReloadScene::GameTime(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReloadScene_GameTime_m36814128F79BEB3040EE91E57A9927F31E7B0E5D (ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8 * __this, bool ___run0, const RuntimeMethod* method)
{
	{
		// if (run)
		bool L_0 = ___run0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Time.timeScale = 1;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		return;
	}

IL_000e:
	{
		// Time.timeScale = 0;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ReloadScene::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReloadScene__ctor_mDDA626CD723DA4B7EE5BD4F0DA2CCAADF3F2081E (ReloadScene_t173F042F54F54D41C710E040CCC2D9993B53C0F8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Rope::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rope_Awake_m5E84CC9E098DDCC82AB23D409A275D52D03A99B9 (Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_mD5BC9EADE1AA529A5299A4D8B020FB49663DAC3A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// lr = GetComponent<LineRenderer>();
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_0;
		L_0 = Component_GetComponent_TisLineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_mD5BC9EADE1AA529A5299A4D8B020FB49663DAC3A(__this, /*hidden argument*/Component_GetComponent_TisLineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_mD5BC9EADE1AA529A5299A4D8B020FB49663DAC3A_RuntimeMethod_var);
		__this->set_lr_4(L_0);
		// }
		return;
	}
}
// System.Void Rope::SetUpLine(UnityEngine.Transform[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rope_SetUpLine_mE457D644AFEA7E498BF232ED5FABB98E00F227C2 (Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * __this, TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___points0, const RuntimeMethod* method)
{
	{
		// lr.positionCount = points.Length;
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_0 = __this->get_lr_4();
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_1 = ___points0;
		LineRenderer_set_positionCount_mA8B73AC2B9151473E94F3713E1041AA34D758AE5(L_0, ((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))), /*hidden argument*/NULL);
		// this.points = points;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_2 = ___points0;
		__this->set_points_5(L_2);
		// }
		return;
	}
}
// System.Void Rope::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rope_Update_mC94A4912E91FE4D823146889234424AA1D24D393 (Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < points.Length; i++)
		V_0 = 0;
		goto IL_0021;
	}

IL_0004:
	{
		// lr.SetPosition(i, points[i].position);
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_0 = __this->get_lr_4();
		int32_t L_1 = V_0;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_2 = __this->get_points_5();
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F(L_0, L_1, L_6, /*hidden argument*/NULL);
		// for (int i = 0; i < points.Length; i++)
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0021:
	{
		// for (int i = 0; i < points.Length; i++)
		int32_t L_8 = V_0;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_9 = __this->get_points_5();
		if ((((int32_t)L_8) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Rope::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rope__ctor_m08A38A25419AAB806FB62BB0DC83606EB1EE518D (Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RopeHost::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RopeHost_Start_m93F757227F9067512D6D553C588BF33AA66C9F45 (RopeHost_t903B27F3928D918D402D4535C94CD5611C450872 * __this, const RuntimeMethod* method)
{
	{
		// rope.SetUpLine(points);
		Rope_t37208DDC038AB00AA9DDC82AC2B0E1D9CF06D4D8 * L_0 = __this->get_rope_5();
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_1 = __this->get_points_4();
		Rope_SetUpLine_mE457D644AFEA7E498BF232ED5FABB98E00F227C2(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void RopeHost::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RopeHost_Update_mFD013478A3639ACB8BB6DE48A41D1B9A7B192527 (RopeHost_t903B27F3928D918D402D4535C94CD5611C450872 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void RopeHost::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RopeHost__ctor_mA80C48A6A1E2513DB360F7291CBBEC3CEA95E23D (RopeHost_t903B27F3928D918D402D4535C94CD5611C450872 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SpawnArrows::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_Start_m6F85F93489E8C02F4D7281A8577AF3823331D616 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void SpawnArrows::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_Update_m94D1056DE5397B5A815FED5968001853351A4E76 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * V_0 = NULL;
	{
		// if (Time.timeScale == 1)
		float L_0;
		L_0 = Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4(/*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(1.0f)))))
		{
			goto IL_0103;
		}
	}
	{
		// if (amountOfArrows > 0 && amountOfArrows < 11)
		int32_t L_1 = __this->get_amountOfArrows_4();
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_2 = __this->get_amountOfArrows_4();
		if ((((int32_t)L_2) >= ((int32_t)((int32_t)11))))
		{
			goto IL_0059;
		}
	}
	{
		// SetLastArrowActive();
		SpawnArrows_SetLastArrowActive_m2001D319F6199B46BB817262C9FB63AD0341C510(__this, /*hidden argument*/NULL);
		// SetAmountOfArrows(amountOfArrows);
		int32_t L_3 = __this->get_amountOfArrows_4();
		SpawnArrows_SetAmountOfArrows_m25A454CB577A1333F928117357B0D17FC998BB8E(__this, L_3, /*hidden argument*/NULL);
		// ArrowStats lastArrow = arrows[amountOfArrows - 1].GetComponent<ArrowStats>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_4 = __this->get_arrows_5();
		int32_t L_5 = __this->get_amountOfArrows_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_4, ((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)1)), /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * L_7;
		L_7 = GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051(L_6, /*hidden argument*/GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051_RuntimeMethod_var);
		V_0 = L_7;
		// GetPressedButton(lastArrow.ArrowDir);
		ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * L_8 = V_0;
		String_t* L_9 = L_8->get_ArrowDir_9();
		SpawnArrows_GetPressedButton_m0966981F8F7708C38832CD6550CA4267CC4C1361(__this, L_9, /*hidden argument*/NULL);
	}

IL_0059:
	{
		// if (hook.isHooked == true)
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_10 = __this->get_hook_12();
		bool L_11 = L_10->get_isHooked_13();
		if (!L_11)
		{
			goto IL_00f7;
		}
	}
	{
		// if (amountOfArrows <= 0)
		int32_t L_12 = __this->get_amountOfArrows_4();
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_009c;
		}
	}
	{
		// hook.isDrop = true;
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_13 = __this->get_hook_12();
		L_13->set_isDrop_10((bool)1);
		// canvs.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = __this->get_canvs_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_14, (bool)0, /*hidden argument*/NULL);
		// hook.LaunchAnimal();
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_15 = __this->get_hook_12();
		Hook_LaunchAnimal_m4A87E70FB9E4B19A7EFF82E506A0FCFF95943ED3(L_15, /*hidden argument*/NULL);
		// ResetArrowsValues();
		SpawnArrows_ResetArrowsValues_mE6914F631A10EACEC79678DEE895E8CD11C75DE3(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_009c:
	{
		// else if (timer.time <= 0)
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_16 = __this->get_timer_10();
		float L_17 = L_16->get_time_6();
		if ((!(((float)L_17) <= ((float)(0.0f)))))
		{
			goto IL_0103;
		}
	}
	{
		// lives.damage++;
		Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * L_18 = __this->get_lives_11();
		Lives_t7DD957ADDF6D3654AB9C5BAC2E16F8823F8F2727 * L_19 = L_18;
		int32_t L_20 = L_19->get_damage_7();
		L_19->set_damage_7(((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1)));
		// canvs.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21 = __this->get_canvs_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_21, (bool)0, /*hidden argument*/NULL);
		// hook.isDrop = true;
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_22 = __this->get_hook_12();
		L_22->set_isDrop_10((bool)1);
		// hook.canGrab = false;
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_23 = __this->get_hook_12();
		L_23->set_canGrab_11((bool)0);
		// hook.LaunchAnimal();
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_24 = __this->get_hook_12();
		Hook_LaunchAnimal_m4A87E70FB9E4B19A7EFF82E506A0FCFF95943ED3(L_24, /*hidden argument*/NULL);
		// ResetArrowsValues();
		SpawnArrows_ResetArrowsValues_mE6914F631A10EACEC79678DEE895E8CD11C75DE3(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00f7:
	{
		// canvs.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = __this->get_canvs_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_25, (bool)0, /*hidden argument*/NULL);
	}

IL_0103:
	{
		// }
		return;
	}
}
// System.Void SpawnArrows::GetPressedButton(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_GetPressedButton_m0966981F8F7708C38832CD6550CA4267CC4C1361 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral378F2A2277727E3F33599E270749A10E7C2B55E1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE917FA4AFDCDAD23459FDCCEB810C2C11F6B1F9C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetKeyDown(KeyCode.RightArrow)&&key=="right")
		bool L_0;
		L_0 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)275), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_1 = ___key0;
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		// amountOfArrows--;
		int32_t L_3 = __this->get_amountOfArrows_4();
		__this->set_amountOfArrows_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1)));
		// click.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_4 = __this->get_click_13();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_4, /*hidden argument*/NULL);
		// }else if (Input.GetKeyDown(KeyCode.LeftArrow) && key == "left")
		return;
	}

IL_0033:
	{
		// }else if (Input.GetKeyDown(KeyCode.LeftArrow) && key == "left")
		bool L_5;
		L_5 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)276), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0066;
		}
	}
	{
		String_t* L_6 = ___key0;
		bool L_7;
		L_7 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		// amountOfArrows--;
		int32_t L_8 = __this->get_amountOfArrows_4();
		__this->set_amountOfArrows_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		// click.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_9 = __this->get_click_13();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_9, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0066:
	{
		// else if (Input.GetKeyDown(KeyCode.UpArrow) && key == "up")
		bool L_10;
		L_10 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)273), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0099;
		}
	}
	{
		String_t* L_11 = ___key0;
		bool L_12;
		L_12 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_11, _stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0099;
		}
	}
	{
		// amountOfArrows--;
		int32_t L_13 = __this->get_amountOfArrows_4();
		__this->set_amountOfArrows_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1)));
		// click.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_14 = __this->get_click_13();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_14, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0099:
	{
		// else if (Input.GetKeyDown(KeyCode.DownArrow) && key == "down")
		bool L_15;
		L_15 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)274), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00cc;
		}
	}
	{
		String_t* L_16 = ___key0;
		bool L_17;
		L_17 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_16, _stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00cc;
		}
	}
	{
		// amountOfArrows--;
		int32_t L_18 = __this->get_amountOfArrows_4();
		__this->set_amountOfArrows_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)1)));
		// click.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_19 = __this->get_click_13();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_19, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00cc:
	{
		// else if (Input.anyKeyDown && amountOfArrows<10&& !Input.GetMouseButtonDown(0))
		bool L_20;
		L_20 = Input_get_anyKeyDown_m0E5B02352AC40A25BBE31F4532764279D5AF03A5(/*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_21 = __this->get_amountOfArrows_4();
		if ((((int32_t)L_21) >= ((int32_t)((int32_t)10))))
		{
			goto IL_015d;
		}
	}
	{
		bool L_22;
		L_22 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(0, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_015d;
		}
	}
	{
		// arrows[amountOfArrows - 1].GetComponent<Animator>().SetBool("Move", false);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_23 = __this->get_arrows_5();
		int32_t L_24 = __this->get_amountOfArrows_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25;
		L_25 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_23, ((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)1)), /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_26;
		L_26 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_25, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_26, _stringLiteral378F2A2277727E3F33599E270749A10E7C2B55E1, (bool)0, /*hidden argument*/NULL);
		// arrows[amountOfArrows - 1].GetComponent<Animator>().Play("WronPress");
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_27 = __this->get_arrows_5();
		int32_t L_28 = __this->get_amountOfArrows_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29;
		L_29 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_27, ((int32_t)il2cpp_codegen_subtract((int32_t)L_28, (int32_t)1)), /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_30;
		L_30 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_29, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_Play_mE5E8B1753FFDF754EAD1ACEFF6C5B6ACA506363C(L_30, _stringLiteralE917FA4AFDCDAD23459FDCCEB810C2C11F6B1F9C, /*hidden argument*/NULL);
		// amountOfArrows++;
		int32_t L_31 = __this->get_amountOfArrows_4();
		__this->set_amountOfArrows_4(((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1)));
		// timer.time += 0.2f;
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_32 = __this->get_timer_10();
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_33 = L_32;
		float L_34 = L_33->get_time_6();
		L_33->set_time_6(((float)il2cpp_codegen_add((float)L_34, (float)(0.200000003f))));
		// falseClick.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_35 = __this->get_falseClick_14();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_35, /*hidden argument*/NULL);
	}

IL_015d:
	{
		// }
		return;
	}
}
// System.Void SpawnArrows::SetLastArrowActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_SetLastArrowActive_m2001D319F6199B46BB817262C9FB63AD0341C510 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral378F2A2277727E3F33599E270749A10E7C2B55E1);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * V_1 = NULL;
	{
		// for (int i = 0; i < arrows.Count; i++)
		V_0 = 0;
		goto IL_004f;
	}

IL_0004:
	{
		// ArrowStats lastArrow = arrows[i].GetComponent<ArrowStats>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = __this->get_arrows_5();
		int32_t L_1 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * L_3;
		L_3 = GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051(L_2, /*hidden argument*/GameObject_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_m8EB92BD773CF640A0C0A8D61E0F0A811EFE33051_RuntimeMethod_var);
		V_1 = L_3;
		// lastArrow.IsActive = false;
		ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * L_4 = V_1;
		L_4->set_IsActive_10((bool)0);
		// if (i == amountOfArrows - 1)
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_amountOfArrows_4();
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1))))))
		{
			goto IL_004b;
		}
	}
	{
		// lastArrow.IsActive = true;
		ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * L_7 = V_1;
		L_7->set_IsActive_10((bool)1);
		// arrows[i].GetComponent<Animator>().SetBool("Move", true);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_8 = __this->get_arrows_5();
		int32_t L_9 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10;
		L_10 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_8, L_9, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_11;
		L_11 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_10, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_11, _stringLiteral378F2A2277727E3F33599E270749A10E7C2B55E1, (bool)1, /*hidden argument*/NULL);
	}

IL_004b:
	{
		// for (int i = 0; i < arrows.Count; i++)
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_004f:
	{
		// for (int i = 0; i < arrows.Count; i++)
		int32_t L_13 = V_0;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_14 = __this->get_arrows_5();
		int32_t L_15;
		L_15 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_14, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void SpawnArrows::SetAmountOfArrows(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_SetAmountOfArrows_m25A454CB577A1333F928117357B0D17FC998BB8E (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, int32_t ___amount0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < arrows.Count; i++)
		V_0 = 0;
		goto IL_0034;
	}

IL_0004:
	{
		// if (amount-1 >= i)
		int32_t L_0 = ___amount0;
		int32_t L_1 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1))) < ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		// arrows[i].SetActive(true);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_2 = __this->get_arrows_5();
		int32_t L_3 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		goto IL_0030;
	}

IL_001e:
	{
		// arrows[i].SetActive(false);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_5 = __this->get_arrows_5();
		int32_t L_6 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// for (int i = 0; i < arrows.Count; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0034:
	{
		// for (int i = 0; i < arrows.Count; i++)
		int32_t L_9 = V_0;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_10 = __this->get_arrows_5();
		int32_t L_11;
		L_11 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_10, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void SpawnArrows::ResetArrowsValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows_ResetArrowsValues_mE6914F631A10EACEC79678DEE895E8CD11C75DE3 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_mDB796D00857A64249229F6FC894C95FB0C030A2A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < arrows.Count; i++)
		V_0 = 0;
		goto IL_0023;
	}

IL_0004:
	{
		// ArrowStats arrow = arrows[i].transform.GetComponent<ArrowStats>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = __this->get_arrows_5();
		int32_t L_1 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_2, /*hidden argument*/NULL);
		ArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578 * L_4;
		L_4 = Component_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_mDB796D00857A64249229F6FC894C95FB0C030A2A(L_3, /*hidden argument*/Component_GetComponent_TisArrowStats_t3EF94FF6B3D4A145B016A8DE0C1528D1B985B578_mDB796D00857A64249229F6FC894C95FB0C030A2A_RuntimeMethod_var);
		// arrow.RandomDir();
		ArrowStats_RandomDir_m097FB8FC53250C2A2BEED7EF0B0C2CCE316E7873(L_4, /*hidden argument*/NULL);
		// for (int i = 0; i < arrows.Count; i++)
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0023:
	{
		// for (int i = 0; i < arrows.Count; i++)
		int32_t L_6 = V_0;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_7 = __this->get_arrows_5();
		int32_t L_8;
		L_8 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_7, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void SpawnArrows::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpawnArrows__ctor_m4F3DA99697B9F6AA06A4B88A3B6584BB95674FB9 (SpawnArrows_t8EF6E0F6ECC7C8ACD19CA298A5897A458ABA5DF8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private List<GameObject> arrows=new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_0, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_arrows_5(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Spawner::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8 (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < animals.Length; i++)
		V_0 = 0;
		goto IL_001b;
	}

IL_0004:
	{
		// startChance.Add(chance[i]);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = __this->get_startChance_12();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = __this->get_chance_11();
		int32_t L_2 = V_0;
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_0, L_4, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// for (int i = 0; i < animals.Length; i++)
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_001b:
	{
		// for (int i = 0; i < animals.Length; i++)
		int32_t L_6 = V_0;
		ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* L_7 = __this->get_animals_9();
		if ((((int32_t)L_6) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// ResetChances();
		Spawner_ResetChances_mA8C792419BBF97274D2E75B89F8B3D65E6F6C8AD(__this, /*hidden argument*/NULL);
		// SpawnAnimal();
		Spawner_SpawnAnimal_mE780A4641654B1587610AF4A62A070E4BB681163(__this, /*hidden argument*/NULL);
		// StartCoroutine(WaitAndPrint(timer));
		float L_8 = __this->get_timer_8();
		RuntimeObject* L_9;
		L_9 = Spawner_WaitAndPrint_m84781AD3E91DCCE5F55370470FEB6EDAAD7A90D6(__this, L_8, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_10;
		L_10 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Spawner::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_Update_m8E44DB2210E6C1692B202D38D0867961E9720AA9 (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Spawner::ResetChances()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_ResetChances_mA8C792419BBF97274D2E75B89F8B3D65E6F6C8AD (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// int headgChanse = 0;
		V_0 = 0;
		// lastChance = 0;
		__this->set_lastChance_10(0);
		// for (int i = 0; i < animals.Length; i++)
		V_1 = 0;
		goto IL_0033;
	}

IL_000d:
	{
		// chance[i] = animals[i].amounOfActiveImages *startChance[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = __this->get_chance_11();
		int32_t L_1 = V_1;
		ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* L_2 = __this->get_animals_9();
		int32_t L_3 = V_1;
		int32_t L_4 = L_3;
		ImageAdder_t6C592C87520D2C288A99A30FBAAC77BAF0E4C281 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		int32_t L_6 = L_5->get_amounOfActiveImages_5();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_7 = __this->get_startChance_12();
		int32_t L_8 = V_1;
		int32_t L_9;
		L_9 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		(L_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_1), (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)L_9)));
		// for (int i = 0; i < animals.Length; i++)
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (int i = 0; i < animals.Length; i++)
		int32_t L_11 = V_1;
		ImageAdderU5BU5D_tA3B1F287B1D32BCDD0AD76DAFD724B570C764E81* L_12 = __this->get_animals_9();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))
		{
			goto IL_000d;
		}
	}
	{
		// for (int i = 0; i < chance.Length-1; i++)
		V_2 = 0;
		goto IL_0051;
	}

IL_0042:
	{
		// headgChanse += chance[i];
		int32_t L_13 = V_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = __this->get_chance_11();
		int32_t L_15 = V_2;
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_16));
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_17));
		// for (int i = 0; i < chance.Length-1; i++)
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0051:
	{
		// for (int i = 0; i < chance.Length-1; i++)
		int32_t L_19 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_20 = __this->get_chance_11();
		if ((((int32_t)L_19) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))), (int32_t)1)))))
		{
			goto IL_0042;
		}
	}
	{
		// chance[chance.Length-1] = headgChanse / 5;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_21 = __this->get_chance_11();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_22 = __this->get_chance_11();
		int32_t L_23 = V_0;
		(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))), (int32_t)1))), (int32_t)((int32_t)((int32_t)L_23/(int32_t)5)));
		// for (int i = 0; i < chance.Length; i++)
		V_3 = 0;
		goto IL_008f;
	}

IL_0076:
	{
		// lastChance += chance[i];
		int32_t L_24 = __this->get_lastChance_10();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_25 = __this->get_chance_11();
		int32_t L_26 = V_3;
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_27));
		__this->set_lastChance_10(((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)L_28)));
		// for (int i = 0; i < chance.Length; i++)
		int32_t L_29 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_008f:
	{
		// for (int i = 0; i < chance.Length; i++)
		int32_t L_30 = V_3;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_31 = __this->get_chance_11();
		if ((((int32_t)L_30) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_31)->max_length))))))
		{
			goto IL_0076;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Spawner::SpawnAnimal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_SpawnAnimal_mE780A4641654B1587610AF4A62A070E4BB681163 (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mDCE712F19A37A792449BA93CB0736F41B4EE17E7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// GameObject animal = null;
		V_0 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		// int rand = Random.Range(0, lastChance + 1);
		int32_t L_0 = __this->get_lastChance_10();
		int32_t L_1;
		L_1 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_1;
		// if (rand <= chance[0])
		int32_t L_2 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_3 = __this->get_chance_11();
		int32_t L_4 = 0;
		int32_t L_5 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		if ((((int32_t)L_2) > ((int32_t)L_5)))
		{
			goto IL_002c;
		}
	}
	{
		// animal = Instantiate(Animals[0]);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_6 = __this->get_Animals_4();
		int32_t L_7 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = (L_6)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_8, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_0 = L_9;
		// }
		goto IL_00aa;
	}

IL_002c:
	{
		// else if (rand > chance[0] && rand <= chance[1]+ chance[0])
		int32_t L_10 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_11 = __this->get_chance_11();
		int32_t L_12 = 0;
		int32_t L_13 = (L_11)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_12));
		if ((((int32_t)L_10) <= ((int32_t)L_13)))
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_14 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_15 = __this->get_chance_11();
		int32_t L_16 = 1;
		int32_t L_17 = (L_15)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_16));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_18 = __this->get_chance_11();
		int32_t L_19 = 0;
		int32_t L_20 = (L_18)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_19));
		if ((((int32_t)L_14) > ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)L_20)))))
		{
			goto IL_005b;
		}
	}
	{
		// animal = Instantiate(Animals[1]);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_21 = __this->get_Animals_4();
		int32_t L_22 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = (L_21)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_22));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24;
		L_24 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_23, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_0 = L_24;
		// }
		goto IL_00aa;
	}

IL_005b:
	{
		// else if (rand > chance[1] + chance[0] && rand <= chance[2] + chance[1] + chance[0])
		int32_t L_25 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_26 = __this->get_chance_11();
		int32_t L_27 = 1;
		int32_t L_28 = (L_26)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_27));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_29 = __this->get_chance_11();
		int32_t L_30 = 0;
		int32_t L_31 = (L_29)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_30));
		if ((((int32_t)L_25) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)L_31)))))
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_32 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_33 = __this->get_chance_11();
		int32_t L_34 = 2;
		int32_t L_35 = (L_33)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_34));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_36 = __this->get_chance_11();
		int32_t L_37 = 1;
		int32_t L_38 = (L_36)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_37));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_39 = __this->get_chance_11();
		int32_t L_40 = 0;
		int32_t L_41 = (L_39)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_40));
		if ((((int32_t)L_32) > ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)L_38)), (int32_t)L_41)))))
		{
			goto IL_009c;
		}
	}
	{
		// animal = Instantiate(Animals[2]);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_42 = __this->get_Animals_4();
		int32_t L_43 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_44 = (L_42)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_43));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_45;
		L_45 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_44, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_0 = L_45;
		// }
		goto IL_00aa;
	}

IL_009c:
	{
		// animal = Instantiate(Animals[3]);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_46 = __this->get_Animals_4();
		int32_t L_47 = 3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_48 = (L_46)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_47));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_49;
		L_49 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_48, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_0 = L_49;
	}

IL_00aa:
	{
		// animal.transform.position = transform.position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_50 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_51;
		L_51 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_50, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_52;
		L_52 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		L_53 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_52, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_51, L_53, /*hidden argument*/NULL);
		// animal.transform.localScale = new Vector3(animal.transform.localScale.x * dir, animal.transform.localScale.y, animal.transform.localScale.z);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_54 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_55;
		L_55 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_54, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_56 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_57;
		L_57 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_56, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_58;
		L_58 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_57, /*hidden argument*/NULL);
		float L_59 = L_58.get_x_2();
		int32_t L_60 = __this->get_dir_6();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_61 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_62;
		L_62 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_61, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_63;
		L_63 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_62, /*hidden argument*/NULL);
		float L_64 = L_63.get_y_3();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_65 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_66;
		L_66 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_65, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_67;
		L_67 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_66, /*hidden argument*/NULL);
		float L_68 = L_67.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_69;
		memset((&L_69), 0, sizeof(L_69));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_69), ((float)il2cpp_codegen_multiply((float)L_59, (float)((float)((float)L_60)))), L_64, L_68, /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_55, L_69, /*hidden argument*/NULL);
		// AnimalWalk = animal.GetComponent<Walk>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_70 = V_0;
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_71;
		L_71 = GameObject_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mDCE712F19A37A792449BA93CB0736F41B4EE17E7(L_70, /*hidden argument*/GameObject_GetComponent_TisWalk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029_mDCE712F19A37A792449BA93CB0736F41B4EE17E7_RuntimeMethod_var);
		__this->set_AnimalWalk_5(L_71);
		// AnimalWalk.side = dir;
		Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * L_72 = __this->get_AnimalWalk_5();
		int32_t L_73 = __this->get_dir_6();
		L_72->set_side_6(L_73);
		// }
		return;
	}
}
// System.Collections.IEnumerator Spawner::WaitAndPrint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Spawner_WaitAndPrint_m84781AD3E91DCCE5F55370470FEB6EDAAD7A90D6 (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, float ___waitTime0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * L_0 = (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 *)il2cpp_codegen_object_new(U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97_il2cpp_TypeInfo_var);
		U3CWaitAndPrintU3Ed__14__ctor_m13628828E6174F666C9D54AF360AA61A53B8FF06(L_0, 0, /*hidden argument*/NULL);
		U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * L_1 = L_0;
		L_1->set_U3CU3E4__this_3(__this);
		U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * L_2 = L_1;
		float L_3 = ___waitTime0;
		L_2->set_waitTime_2(L_3);
		return L_2;
	}
}
// System.Void Spawner::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C (Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private List<int> startChance = new List<int>();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_0, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		__this->set_startChance_12(L_0);
		// [SerializeField] private int[] spawnTime=new int[2];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)2);
		__this->set_spawnTime_13(L_1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Timer::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timer_Start_mF356578A412E0D4BE318BC567718C7DBD90E05F0 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Timer::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timer_OnEnable_m46932BE65908389800B77332F5F6FDA0BF3C596B (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, const RuntimeMethod* method)
{
	{
		// sound.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_sound_8();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_0, /*hidden argument*/NULL);
		// StartCoroutine(ChangePitch(0.1f));
		RuntimeObject* L_1;
		L_1 = Timer_ChangePitch_m27DC73EAF0836A0D0ED3567498BAC3850E8C5ED5(__this, (0.100000001f), /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_2;
		L_2 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Timer::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timer_OnDisable_mD522C218198DAA511E56B09CABB7EA58022E8FCB (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3DEB08645ED85BEE45AC44E1C4021FE85CE537F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// sound.Pause();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_sound_8();
		AudioSource_Pause_mC4F9932A77B6AA2CC3FB720721B7837CF57B675D(L_0, /*hidden argument*/NULL);
		// sound.pitch = 1;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = __this->get_sound_8();
		AudioSource_set_pitch_m9E9EEFADFAB44ADEC656573425CF34D0069A0CBC(L_1, (1.0f), /*hidden argument*/NULL);
		// StopCoroutine("ChangePitch");
		MonoBehaviour_StopCoroutine_m4DB2A899F9BDF8CA3264DD8C4130E767702B626B(__this, _stringLiteral3DEB08645ED85BEE45AC44E1C4021FE85CE537F8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Timer::SetTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timer_SetTime_m2C010FEA2C0A8AACB10D45D4829A1AA8D2A8FC83 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, float ___setTime0, const RuntimeMethod* method)
{
	{
		// time = setTime;
		float L_0 = ___setTime0;
		__this->set_time_6(L_0);
		// localTime = setTime;
		float L_1 = ___setTime0;
		__this->set_localTime_7(L_1);
		// pitching = time;
		float L_2 = __this->get_time_6();
		__this->set_pitching_9(L_2);
		// StartCoroutine(TimerStart(0.1f));
		RuntimeObject* L_3;
		L_3 = Timer_TimerStart_m2A9D651E9D42FA173C24B884EB81F3D181D6D558(__this, (0.100000001f), /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_4;
		L_4 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Timer::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timer_Update_m336594DF06E073C9CC317142E46E02AFC94A026C (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, const RuntimeMethod* method)
{
	{
		// if (isTime == true)
		bool L_0 = __this->get_isTime_5();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// main.fillAmount = (time/ localTime);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_1 = __this->get_main_4();
		float L_2 = __this->get_time_6();
		float L_3 = __this->get_localTime_7();
		Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C(L_1, ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
	}

IL_0020:
	{
		// if (time > localTime)
		float L_4 = __this->get_time_6();
		float L_5 = __this->get_localTime_7();
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_003a;
		}
	}
	{
		// time = localTime;
		float L_6 = __this->get_localTime_7();
		__this->set_time_6(L_6);
	}

IL_003a:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator Timer::ChangePitch(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Timer_ChangePitch_m27DC73EAF0836A0D0ED3567498BAC3850E8C5ED5 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, float ___waitTime0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * L_0 = (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 *)il2cpp_codegen_object_new(U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9_il2cpp_TypeInfo_var);
		U3CChangePitchU3Ed__11__ctor_m2801AE84732F6D9E51B98C1C637A4C32B9652F5D(L_0, 0, /*hidden argument*/NULL);
		U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * L_1 = L_0;
		L_1->set_U3CU3E4__this_3(__this);
		U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * L_2 = L_1;
		float L_3 = ___waitTime0;
		L_2->set_waitTime_2(L_3);
		return L_2;
	}
}
// System.Collections.IEnumerator Timer::TimerStart(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Timer_TimerStart_m2A9D651E9D42FA173C24B884EB81F3D181D6D558 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, float ___waitTime0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * L_0 = (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E *)il2cpp_codegen_object_new(U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E_il2cpp_TypeInfo_var);
		U3CTimerStartU3Ed__12__ctor_m1C21FE74F4545C01E22A3A35B87ABDD93BB0E1FB(L_0, 0, /*hidden argument*/NULL);
		U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * L_1 = L_0;
		L_1->set_U3CU3E4__this_3(__this);
		U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * L_2 = L_1;
		float L_3 = ___waitTime0;
		L_2->set_waitTime_2(L_3);
		return L_2;
	}
}
// System.Void Timer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timer__ctor_m74709038BC88FE71F7D6C06D0FF352FBE17410E8 (Timer_t32ADF3155A88C37628B80D227C821651666DD405 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Walk::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk_Start_m09A7D2B2F39B6784AAE0BE9D285D04A5CD593585 (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// main =  gameObject.GetComponent<Transform>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_0, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		__this->set_main_4(L_1);
		// }
		return;
	}
}
// System.Void Walk::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk_OnEnable_mE9BDC27A7B11ABB72556F87FFE85E9F62A1F9C95 (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, const RuntimeMethod* method)
{
	{
		// walk.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_walk_7();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Walk::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk_OnDisable_mFE7BC58553D49A6BC3C7D1E3392D74E1AE1987B1 (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, const RuntimeMethod* method)
{
	{
		// walk.Stop();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_walk_7();
		AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Walk::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk_Update_m574B5C3EE3EAE9BD3F44BB5F1CF1751D40E84468 (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, const RuntimeMethod* method)
{
	{
		// main.position += new Vector3(1,0,0) * speed * side*Time.deltaTime;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_main_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = L_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		float L_4 = __this->get_speed_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_3, L_4, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_side_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_5, ((float)((float)L_6)), /*hidden argument*/NULL);
		float L_8;
		L_8 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_7, L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_2, L_9, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_1, L_10, /*hidden argument*/NULL);
		// if(Time.timeScale==0)
		float L_11;
		L_11 = Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4(/*hidden argument*/NULL);
		if ((!(((float)L_11) == ((float)(0.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		// walk.Stop();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_12 = __this->get_walk_7();
		AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F(L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		// }
		return;
	}
}
// System.Void Walk::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk_OnTriggerEnter2D_mC911A427400A6F72CC308F28E9E86E037B3867C4 (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8FCD9DFEDB4B5B0CC0587121C4AB1682B834E22B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB75428E1AE616B1DD1D25315981C50AB5563C4DE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Hook"&& transform.parent!=null)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteral8FCD9DFEDB4B5B0CC0587121C4AB1682B834E22B, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		// transform.GetComponent<Animator>().SetBool("Caught", true);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_7;
		L_7 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(L_6, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_7, _stringLiteralB75428E1AE616B1DD1D25315981C50AB5563C4DE, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void Walk::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk_OnTriggerStay2D_mA14677244C152E766D3B96F35D98F50507217411 (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8FCD9DFEDB4B5B0CC0587121C4AB1682B834E22B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB75428E1AE616B1DD1D25315981C50AB5563C4DE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Hook" && transform.parent != null)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteral8FCD9DFEDB4B5B0CC0587121C4AB1682B834E22B, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		// transform.GetComponent<Animator>().SetBool("Caught", true);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_7;
		L_7 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(L_6, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_7, _stringLiteralB75428E1AE616B1DD1D25315981C50AB5563C4DE, (bool)1, /*hidden argument*/NULL);
		// walk.Stop();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_8 = __this->get_walk_7();
		AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F(L_8, /*hidden argument*/NULL);
	}

IL_0046:
	{
		// }
		return;
	}
}
// System.Void Walk::OnTriggerExit2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk_OnTriggerExit2D_mF49820AA9887920809700E429419FE9965FAFD0C (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8FCD9DFEDB4B5B0CC0587121C4AB1682B834E22B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB75428E1AE616B1DD1D25315981C50AB5563C4DE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (collision.tag == "Hook" )
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collision0;
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteral8FCD9DFEDB4B5B0CC0587121C4AB1682B834E22B, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		// transform.GetComponent<Animator>().SetBool("Caught", false);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4;
		L_4 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(L_3, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_4, _stringLiteralB75428E1AE616B1DD1D25315981C50AB5563C4DE, (bool)0, /*hidden argument*/NULL);
		// walk.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_5 = __this->get_walk_7();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_5, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// }
		return;
	}
}
// System.Void Walk::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Walk__ctor_m0071C19F2A207DCFA448CA24805BF25D39A014F7 (Walk_t9500137CA5D33AA529C24BEAD97EFE4EA8604029 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Hook/<SetAnimalPaternt>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetAnimalPaterntU3Ed__25__ctor_mB742D425984D061D0168939974145056BD1DC1B3 (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Hook/<SetAnimalPaternt>d__25::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetAnimalPaterntU3Ed__25_System_IDisposable_Dispose_mC9141234784CC5EEDBDE65696C3DB05986773A03 (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Hook/<SetAnimalPaternt>d__25::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSetAnimalPaterntU3Ed__25_MoveNext_m653F8015AD50EE757BABDF3C5968F6FA15FA2739 (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_005d;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(0.3f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_4 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_4, (0.300000012f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0039:
	{
		__this->set_U3CU3E1__state_0((-1));
		// animal.transform.SetParent(transform);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_5 = __this->get_animal_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_5, /*hidden argument*/NULL);
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_7 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_6, L_8, /*hidden argument*/NULL);
		// canSet = false;
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_9 = V_1;
		L_9->set_canSet_18((bool)0);
	}

IL_005d:
	{
		// while (canSet)
		Hook_tB3609EB60D36D31B9D86A7D168B574B69C485310 * L_10 = V_1;
		bool L_11 = L_10->get_canSet_18();
		if (L_11)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Hook/<SetAnimalPaternt>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetAnimalPaterntU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE1A64F7ADA0306BA74D80C6DBDEE29BB21E1B21 (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Hook/<SetAnimalPaternt>d__25::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_Reset_mB0193444DB856035BD3981DE8F420A1B20BED1F4 (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_Reset_mB0193444DB856035BD3981DE8F420A1B20BED1F4_RuntimeMethod_var)));
	}
}
// System.Object Hook/<SetAnimalPaternt>d__25::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_get_Current_mEA7F2410B6D6EB7169A167D765272A2284E3BD98 (U3CSetAnimalPaterntU3Ed__25_tAFFEB24C7A5687903311C31415007EF66C124835 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Spawner/<WaitAndPrint>d__14::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitAndPrintU3Ed__14__ctor_m13628828E6174F666C9D54AF360AA61A53B8FF06 (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Spawner/<WaitAndPrint>d__14::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitAndPrintU3Ed__14_System_IDisposable_Dispose_m1898DEDB299AE15F0E53D5DFAB25C67808A8B5E7 (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Spawner/<WaitAndPrint>d__14::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CWaitAndPrintU3Ed__14_MoveNext_m92DD1C54A5A036567CC200546B38A727BD2E784E (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_001e:
	{
		// yield return new WaitForSeconds(waitTime);
		float L_4 = __this->get_waitTime_2();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0038:
	{
		__this->set_U3CU3E1__state_0((-1));
		// SpawnAnimal();
		Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * L_6 = V_1;
		Spawner_SpawnAnimal_mE780A4641654B1587610AF4A62A070E4BB681163(L_6, /*hidden argument*/NULL);
		// ResetChances();
		Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * L_7 = V_1;
		Spawner_ResetChances_mA8C792419BBF97274D2E75B89F8B3D65E6F6C8AD(L_7, /*hidden argument*/NULL);
		// timer = Random.Range(spawnTime[0], spawnTime[1]);
		Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * L_8 = V_1;
		Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * L_9 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_10 = L_9->get_spawnTime_13();
		int32_t L_11 = 0;
		int32_t L_12 = (L_10)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_11));
		Spawner_t25C5C437F30ECF728D29E0B315C67659B99BEA87 * L_13 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = L_13->get_spawnTime_13();
		int32_t L_15 = 1;
		int32_t L_16 = (L_14)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17;
		L_17 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(L_12, L_16, /*hidden argument*/NULL);
		L_8->set_timer_8(((float)((float)L_17)));
		// while (true)
		goto IL_001e;
	}
}
// System.Object Spawner/<WaitAndPrint>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitAndPrintU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4443DD1EE085AE8E2853718C0F4D215560CA8E71 (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Spawner/<WaitAndPrint>d__14::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_Reset_m5F0E9697F961B13CC552FD329B4005C8AF9F76DE (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_Reset_m5F0E9697F961B13CC552FD329B4005C8AF9F76DE_RuntimeMethod_var)));
	}
}
// System.Object Spawner/<WaitAndPrint>d__14::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_get_Current_mABEF781BFDB876A1E0EE81333EA325EFAFD974E0 (U3CWaitAndPrintU3Ed__14_t13F4C70E0493DB189EFE207F0376ED506A559B97 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Timer/<ChangePitch>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangePitchU3Ed__11__ctor_m2801AE84732F6D9E51B98C1C637A4C32B9652F5D (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Timer/<ChangePitch>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangePitchU3Ed__11_System_IDisposable_Dispose_mE46803F6C93BACD92C598F801527C0F2C89B7D3E (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Timer/<ChangePitch>d__11::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CChangePitchU3Ed__11_MoveNext_m798294331D66B49C937F5AD740DD86A92EDE9023 (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timer_t32ADF3155A88C37628B80D227C821651666DD405 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_001e:
	{
		// yield return new WaitForSeconds(waitTime);
		float L_4 = __this->get_waitTime_2();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0038:
	{
		__this->set_U3CU3E1__state_0((-1));
		// sound.pitch = time / pitching;
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_6 = V_1;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_7 = L_6->get_sound_8();
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_8 = V_1;
		float L_9 = L_8->get_time_6();
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_10 = V_1;
		float L_11 = L_10->get_pitching_9();
		AudioSource_set_pitch_m9E9EEFADFAB44ADEC656573425CF34D0069A0CBC(L_7, ((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		// while (true)
		goto IL_001e;
	}
}
// System.Object Timer/<ChangePitch>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangePitchU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BBB9674CB3908CC7B7FB10F4B694887B688E838 (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Timer/<ChangePitch>d__11::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangePitchU3Ed__11_System_Collections_IEnumerator_Reset_mB0435CFEF66344023B0C95E4D331B7443E97BB86 (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CChangePitchU3Ed__11_System_Collections_IEnumerator_Reset_mB0435CFEF66344023B0C95E4D331B7443E97BB86_RuntimeMethod_var)));
	}
}
// System.Object Timer/<ChangePitch>d__11::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangePitchU3Ed__11_System_Collections_IEnumerator_get_Current_mCE2E20C669A79B9F2BB72CCDA8B9E4F83B3EE9F3 (U3CChangePitchU3Ed__11_tD4E17C03920E8754422C37AE68AA8E4E592CE8B9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Timer/<TimerStart>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTimerStartU3Ed__12__ctor_m1C21FE74F4545C01E22A3A35B87ABDD93BB0E1FB (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Timer/<TimerStart>d__12::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTimerStartU3Ed__12_System_IDisposable_Dispose_m1C4A3195C7A57ADC4D8985619F95C2F7D8067660 (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Timer/<TimerStart>d__12::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTimerStartU3Ed__12_MoveNext_m65C48DA04C661F798E970AC86A070BE95B970AB6 (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timer_t32ADF3155A88C37628B80D227C821651666DD405 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_001e:
	{
		// yield return new WaitForSeconds(waitTime);
		float L_4 = __this->get_waitTime_2();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0038:
	{
		__this->set_U3CU3E1__state_0((-1));
		// time -= 0.1f;
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_6 = V_1;
		Timer_t32ADF3155A88C37628B80D227C821651666DD405 * L_7 = V_1;
		float L_8 = L_7->get_time_6();
		L_6->set_time_6(((float)il2cpp_codegen_subtract((float)L_8, (float)(0.100000001f))));
		// while (true)
		goto IL_001e;
	}
}
// System.Object Timer/<TimerStart>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTimerStartU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF8F9C60991ACF051917A4A516E6923E41C31EFD (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Timer/<TimerStart>d__12::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTimerStartU3Ed__12_System_Collections_IEnumerator_Reset_mB691905ECD7D3D98AD081479657EABBDC25EF95F (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTimerStartU3Ed__12_System_Collections_IEnumerator_Reset_mB691905ECD7D3D98AD081479657EABBDC25EF95F_RuntimeMethod_var)));
	}
}
// System.Object Timer/<TimerStart>d__12::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTimerStartU3Ed__12_System_Collections_IEnumerator_get_Current_m05419DFA7E918EAEE2DDB0EEF141D75587276483 (U3CTimerStartU3Ed__12_tAD82439EBE14C913E7D5E69919F4F0768DEB2A8E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get__items_1();
		int32_t L_3 = ___index0;
		int32_t L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_2, (int32_t)L_3);
		return (int32_t)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
