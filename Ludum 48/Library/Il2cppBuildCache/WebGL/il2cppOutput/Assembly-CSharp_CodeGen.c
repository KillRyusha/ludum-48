﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ArrowStats::OnEnable()
extern void ArrowStats_OnEnable_m85CA157B9B0ECEC641DB4F28506FE456DAF562B9 (void);
// 0x00000002 System.Void ArrowStats::Start()
extern void ArrowStats_Start_m9306E8DEB76F28D798461E844AEBD8FA79BBAD99 (void);
// 0x00000003 System.Void ArrowStats::Update()
extern void ArrowStats_Update_mF97643EAE181D86207D4055B549EAA532F08F23C (void);
// 0x00000004 System.Void ArrowStats::RandomDir()
extern void ArrowStats_RandomDir_m097FB8FC53250C2A2BEED7EF0B0C2CCE316E7873 (void);
// 0x00000005 System.Void ArrowStats::.ctor()
extern void ArrowStats__ctor_m44A6A13BE4C29C7C576FD54B4E9BAD7BFDF7D9FE (void);
// 0x00000006 System.Void Border::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Border_OnTriggerEnter2D_mF00A6CA6D75E8C330ECDDD95389E1125C9529256 (void);
// 0x00000007 System.Void Border::.ctor()
extern void Border__ctor_mC47B2974B694148D0553A8B1BA49C1C06A4CB974 (void);
// 0x00000008 System.Void ChangeMenu::Change()
extern void ChangeMenu_Change_mB9D17789278243192BDDE3F04EFBCFBC1DD4A5B3 (void);
// 0x00000009 System.Void ChangeMenu::ClearTutor()
extern void ChangeMenu_ClearTutor_m51B62CD977BA017AB8DE25E3033B6C2A27C51A51 (void);
// 0x0000000A System.Void ChangeMenu::.ctor()
extern void ChangeMenu__ctor_m8590CABB0EE49A4F89D6AE45EAABA1929A2C85AF (void);
// 0x0000000B System.Void ChangeVolume::SetSound(System.Single)
extern void ChangeVolume_SetSound_m7C9168882B9895E62C1018BCE3CD15EC74721193 (void);
// 0x0000000C System.Void ChangeVolume::SetMusic(System.Single)
extern void ChangeVolume_SetMusic_m0D15100E9B1178AAC34CD8C7D4969605E36527BF (void);
// 0x0000000D System.Void ChangeVolume::.ctor()
extern void ChangeVolume__ctor_mEF39DA9624C132587BC057E45EDA63FFB21968F6 (void);
// 0x0000000E System.Void Counter::Start()
extern void Counter_Start_m77A2B23760AA7DA12C9BBEE742221AC01B67AFB3 (void);
// 0x0000000F System.Void Counter::Update()
extern void Counter_Update_m750B99E43903C32501752EDB091B76D4D8591063 (void);
// 0x00000010 System.Void Counter::AnimalCollected(System.Int32)
extern void Counter_AnimalCollected_m350E7A131AFE7EB7EC5619D2F854469B23C6A94D (void);
// 0x00000011 System.Void Counter::.ctor()
extern void Counter__ctor_m12F27AA8C9EDE176E9543076DB214639246AAC73 (void);
// 0x00000012 System.Void Hook::Start()
extern void Hook_Start_m24B908B8FD7C19B750AC841EE5FAE4A36339A4F1 (void);
// 0x00000013 System.Void Hook::Update()
extern void Hook_Update_m754647819A7215B7620FF44950F52962DB13FD76 (void);
// 0x00000014 System.Void Hook::LaunchAnimal()
extern void Hook_LaunchAnimal_m4A87E70FB9E4B19A7EFF82E506A0FCFF95943ED3 (void);
// 0x00000015 System.Void Hook::AnimalSetUp(UnityEngine.Collider2D,System.Int32,System.Single)
extern void Hook_AnimalSetUp_m7197382F476EA370FA9F2BDA2F4B851B497ECA34 (void);
// 0x00000016 System.Void Hook::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Hook_OnTriggerEnter2D_m29375DE9BB6C0820DDA4B0AA450BAEDC4DFE4347 (void);
// 0x00000017 System.Collections.IEnumerator Hook::SetAnimalPaternt(UnityEngine.Collider2D)
extern void Hook_SetAnimalPaternt_m2253BC223C7FD0DFF8372BBD4DDFB9EA30CC6344 (void);
// 0x00000018 System.Void Hook::.ctor()
extern void Hook__ctor_m693B9FDDE2C931A45DDC1B9A1C28EB7915678254 (void);
// 0x00000019 System.Void Hook/<SetAnimalPaternt>d__25::.ctor(System.Int32)
extern void U3CSetAnimalPaterntU3Ed__25__ctor_mB742D425984D061D0168939974145056BD1DC1B3 (void);
// 0x0000001A System.Void Hook/<SetAnimalPaternt>d__25::System.IDisposable.Dispose()
extern void U3CSetAnimalPaterntU3Ed__25_System_IDisposable_Dispose_mC9141234784CC5EEDBDE65696C3DB05986773A03 (void);
// 0x0000001B System.Boolean Hook/<SetAnimalPaternt>d__25::MoveNext()
extern void U3CSetAnimalPaterntU3Ed__25_MoveNext_m653F8015AD50EE757BABDF3C5968F6FA15FA2739 (void);
// 0x0000001C System.Object Hook/<SetAnimalPaternt>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetAnimalPaterntU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE1A64F7ADA0306BA74D80C6DBDEE29BB21E1B21 (void);
// 0x0000001D System.Void Hook/<SetAnimalPaternt>d__25::System.Collections.IEnumerator.Reset()
extern void U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_Reset_mB0193444DB856035BD3981DE8F420A1B20BED1F4 (void);
// 0x0000001E System.Object Hook/<SetAnimalPaternt>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_get_Current_mEA7F2410B6D6EB7169A167D765272A2284E3BD98 (void);
// 0x0000001F System.Void ImageAdder::Awake()
extern void ImageAdder_Awake_m9E8C22521946B3CCA47744E7F7C40900BE67FA1D (void);
// 0x00000020 System.Void ImageAdder::Start()
extern void ImageAdder_Start_m458792536E8897B7C4E56278B5EC120A28E89BE4 (void);
// 0x00000021 System.Void ImageAdder::SetValue(System.Int32)
extern void ImageAdder_SetValue_mA5A8358C7F021A158DB9C2416EB363BBDA478550 (void);
// 0x00000022 System.Void ImageAdder::RemoveImage(System.Int32)
extern void ImageAdder_RemoveImage_mDAE0E516DA388FB5D4EFE3C8DA48194C8A5AF7DA (void);
// 0x00000023 System.Void ImageAdder::Update()
extern void ImageAdder_Update_m35A08A2C257A766E998BDF05BCFC7F570CF1987F (void);
// 0x00000024 System.Void ImageAdder::.ctor()
extern void ImageAdder__ctor_m6709E21B8CDF0D20E75DC9E46607ABDC6A4E3B4A (void);
// 0x00000025 System.Void Lives::Update()
extern void Lives_Update_m4FF28CBFFB9B3F0A5E85BF9308691C8682AFF75D (void);
// 0x00000026 System.Void Lives::ChangeHealth()
extern void Lives_ChangeHealth_mE2A807665CD8C1A4B8FAE8D11DC054164CE391E3 (void);
// 0x00000027 System.Void Lives::.ctor()
extern void Lives__ctor_m77E2BF995DBECEBFB9B04F92EE9C0863AC438AEC (void);
// 0x00000028 System.Void ReloadScene::Start()
extern void ReloadScene_Start_mC5ABE315E40286FE314F4F60294A4FA5A5EEE540 (void);
// 0x00000029 System.Void ReloadScene::Update()
extern void ReloadScene_Update_mB265B9700FC2A03B8C5C3D496FF6FC3B070E16D2 (void);
// 0x0000002A System.Void ReloadScene::LoadScene(System.Int32)
extern void ReloadScene_LoadScene_mB265DE58BB90E668D1369936550B4439184E540B (void);
// 0x0000002B System.Void ReloadScene::ReLoad()
extern void ReloadScene_ReLoad_mC5D67DAECB09026B8AAE83DAF21510F6128DE45E (void);
// 0x0000002C System.Void ReloadScene::GameTime(System.Boolean)
extern void ReloadScene_GameTime_m36814128F79BEB3040EE91E57A9927F31E7B0E5D (void);
// 0x0000002D System.Void ReloadScene::.ctor()
extern void ReloadScene__ctor_mDDA626CD723DA4B7EE5BD4F0DA2CCAADF3F2081E (void);
// 0x0000002E System.Void Rope::Awake()
extern void Rope_Awake_m5E84CC9E098DDCC82AB23D409A275D52D03A99B9 (void);
// 0x0000002F System.Void Rope::SetUpLine(UnityEngine.Transform[])
extern void Rope_SetUpLine_mE457D644AFEA7E498BF232ED5FABB98E00F227C2 (void);
// 0x00000030 System.Void Rope::Update()
extern void Rope_Update_mC94A4912E91FE4D823146889234424AA1D24D393 (void);
// 0x00000031 System.Void Rope::.ctor()
extern void Rope__ctor_m08A38A25419AAB806FB62BB0DC83606EB1EE518D (void);
// 0x00000032 System.Void RopeHost::Start()
extern void RopeHost_Start_m93F757227F9067512D6D553C588BF33AA66C9F45 (void);
// 0x00000033 System.Void RopeHost::Update()
extern void RopeHost_Update_mFD013478A3639ACB8BB6DE48A41D1B9A7B192527 (void);
// 0x00000034 System.Void RopeHost::.ctor()
extern void RopeHost__ctor_mA80C48A6A1E2513DB360F7291CBBEC3CEA95E23D (void);
// 0x00000035 System.Void SpawnArrows::Start()
extern void SpawnArrows_Start_m6F85F93489E8C02F4D7281A8577AF3823331D616 (void);
// 0x00000036 System.Void SpawnArrows::Update()
extern void SpawnArrows_Update_m94D1056DE5397B5A815FED5968001853351A4E76 (void);
// 0x00000037 System.Void SpawnArrows::GetPressedButton(System.String)
extern void SpawnArrows_GetPressedButton_m0966981F8F7708C38832CD6550CA4267CC4C1361 (void);
// 0x00000038 System.Void SpawnArrows::SetLastArrowActive()
extern void SpawnArrows_SetLastArrowActive_m2001D319F6199B46BB817262C9FB63AD0341C510 (void);
// 0x00000039 System.Void SpawnArrows::SetAmountOfArrows(System.Int32)
extern void SpawnArrows_SetAmountOfArrows_m25A454CB577A1333F928117357B0D17FC998BB8E (void);
// 0x0000003A System.Void SpawnArrows::ResetArrowsValues()
extern void SpawnArrows_ResetArrowsValues_mE6914F631A10EACEC79678DEE895E8CD11C75DE3 (void);
// 0x0000003B System.Void SpawnArrows::.ctor()
extern void SpawnArrows__ctor_m4F3DA99697B9F6AA06A4B88A3B6584BB95674FB9 (void);
// 0x0000003C System.Void Spawner::Start()
extern void Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8 (void);
// 0x0000003D System.Void Spawner::Update()
extern void Spawner_Update_m8E44DB2210E6C1692B202D38D0867961E9720AA9 (void);
// 0x0000003E System.Void Spawner::ResetChances()
extern void Spawner_ResetChances_mA8C792419BBF97274D2E75B89F8B3D65E6F6C8AD (void);
// 0x0000003F System.Void Spawner::SpawnAnimal()
extern void Spawner_SpawnAnimal_mE780A4641654B1587610AF4A62A070E4BB681163 (void);
// 0x00000040 System.Collections.IEnumerator Spawner::WaitAndPrint(System.Single)
extern void Spawner_WaitAndPrint_m84781AD3E91DCCE5F55370470FEB6EDAAD7A90D6 (void);
// 0x00000041 System.Void Spawner::.ctor()
extern void Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C (void);
// 0x00000042 System.Void Spawner/<WaitAndPrint>d__14::.ctor(System.Int32)
extern void U3CWaitAndPrintU3Ed__14__ctor_m13628828E6174F666C9D54AF360AA61A53B8FF06 (void);
// 0x00000043 System.Void Spawner/<WaitAndPrint>d__14::System.IDisposable.Dispose()
extern void U3CWaitAndPrintU3Ed__14_System_IDisposable_Dispose_m1898DEDB299AE15F0E53D5DFAB25C67808A8B5E7 (void);
// 0x00000044 System.Boolean Spawner/<WaitAndPrint>d__14::MoveNext()
extern void U3CWaitAndPrintU3Ed__14_MoveNext_m92DD1C54A5A036567CC200546B38A727BD2E784E (void);
// 0x00000045 System.Object Spawner/<WaitAndPrint>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndPrintU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4443DD1EE085AE8E2853718C0F4D215560CA8E71 (void);
// 0x00000046 System.Void Spawner/<WaitAndPrint>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_Reset_m5F0E9697F961B13CC552FD329B4005C8AF9F76DE (void);
// 0x00000047 System.Object Spawner/<WaitAndPrint>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_get_Current_mABEF781BFDB876A1E0EE81333EA325EFAFD974E0 (void);
// 0x00000048 System.Void Timer::Start()
extern void Timer_Start_mF356578A412E0D4BE318BC567718C7DBD90E05F0 (void);
// 0x00000049 System.Void Timer::OnEnable()
extern void Timer_OnEnable_m46932BE65908389800B77332F5F6FDA0BF3C596B (void);
// 0x0000004A System.Void Timer::OnDisable()
extern void Timer_OnDisable_mD522C218198DAA511E56B09CABB7EA58022E8FCB (void);
// 0x0000004B System.Void Timer::SetTime(System.Single)
extern void Timer_SetTime_m2C010FEA2C0A8AACB10D45D4829A1AA8D2A8FC83 (void);
// 0x0000004C System.Void Timer::Update()
extern void Timer_Update_m336594DF06E073C9CC317142E46E02AFC94A026C (void);
// 0x0000004D System.Collections.IEnumerator Timer::ChangePitch(System.Single)
extern void Timer_ChangePitch_m27DC73EAF0836A0D0ED3567498BAC3850E8C5ED5 (void);
// 0x0000004E System.Collections.IEnumerator Timer::TimerStart(System.Single)
extern void Timer_TimerStart_m2A9D651E9D42FA173C24B884EB81F3D181D6D558 (void);
// 0x0000004F System.Void Timer::.ctor()
extern void Timer__ctor_m74709038BC88FE71F7D6C06D0FF352FBE17410E8 (void);
// 0x00000050 System.Void Timer/<ChangePitch>d__11::.ctor(System.Int32)
extern void U3CChangePitchU3Ed__11__ctor_m2801AE84732F6D9E51B98C1C637A4C32B9652F5D (void);
// 0x00000051 System.Void Timer/<ChangePitch>d__11::System.IDisposable.Dispose()
extern void U3CChangePitchU3Ed__11_System_IDisposable_Dispose_mE46803F6C93BACD92C598F801527C0F2C89B7D3E (void);
// 0x00000052 System.Boolean Timer/<ChangePitch>d__11::MoveNext()
extern void U3CChangePitchU3Ed__11_MoveNext_m798294331D66B49C937F5AD740DD86A92EDE9023 (void);
// 0x00000053 System.Object Timer/<ChangePitch>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangePitchU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BBB9674CB3908CC7B7FB10F4B694887B688E838 (void);
// 0x00000054 System.Void Timer/<ChangePitch>d__11::System.Collections.IEnumerator.Reset()
extern void U3CChangePitchU3Ed__11_System_Collections_IEnumerator_Reset_mB0435CFEF66344023B0C95E4D331B7443E97BB86 (void);
// 0x00000055 System.Object Timer/<ChangePitch>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CChangePitchU3Ed__11_System_Collections_IEnumerator_get_Current_mCE2E20C669A79B9F2BB72CCDA8B9E4F83B3EE9F3 (void);
// 0x00000056 System.Void Timer/<TimerStart>d__12::.ctor(System.Int32)
extern void U3CTimerStartU3Ed__12__ctor_m1C21FE74F4545C01E22A3A35B87ABDD93BB0E1FB (void);
// 0x00000057 System.Void Timer/<TimerStart>d__12::System.IDisposable.Dispose()
extern void U3CTimerStartU3Ed__12_System_IDisposable_Dispose_m1C4A3195C7A57ADC4D8985619F95C2F7D8067660 (void);
// 0x00000058 System.Boolean Timer/<TimerStart>d__12::MoveNext()
extern void U3CTimerStartU3Ed__12_MoveNext_m65C48DA04C661F798E970AC86A070BE95B970AB6 (void);
// 0x00000059 System.Object Timer/<TimerStart>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimerStartU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF8F9C60991ACF051917A4A516E6923E41C31EFD (void);
// 0x0000005A System.Void Timer/<TimerStart>d__12::System.Collections.IEnumerator.Reset()
extern void U3CTimerStartU3Ed__12_System_Collections_IEnumerator_Reset_mB691905ECD7D3D98AD081479657EABBDC25EF95F (void);
// 0x0000005B System.Object Timer/<TimerStart>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CTimerStartU3Ed__12_System_Collections_IEnumerator_get_Current_m05419DFA7E918EAEE2DDB0EEF141D75587276483 (void);
// 0x0000005C System.Void Walk::Start()
extern void Walk_Start_m09A7D2B2F39B6784AAE0BE9D285D04A5CD593585 (void);
// 0x0000005D System.Void Walk::OnEnable()
extern void Walk_OnEnable_mE9BDC27A7B11ABB72556F87FFE85E9F62A1F9C95 (void);
// 0x0000005E System.Void Walk::OnDisable()
extern void Walk_OnDisable_mFE7BC58553D49A6BC3C7D1E3392D74E1AE1987B1 (void);
// 0x0000005F System.Void Walk::Update()
extern void Walk_Update_m574B5C3EE3EAE9BD3F44BB5F1CF1751D40E84468 (void);
// 0x00000060 System.Void Walk::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Walk_OnTriggerEnter2D_mC911A427400A6F72CC308F28E9E86E037B3867C4 (void);
// 0x00000061 System.Void Walk::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Walk_OnTriggerStay2D_mA14677244C152E766D3B96F35D98F50507217411 (void);
// 0x00000062 System.Void Walk::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Walk_OnTriggerExit2D_mF49820AA9887920809700E429419FE9965FAFD0C (void);
// 0x00000063 System.Void Walk::.ctor()
extern void Walk__ctor_m0071C19F2A207DCFA448CA24805BF25D39A014F7 (void);
static Il2CppMethodPointer s_methodPointers[99] = 
{
	ArrowStats_OnEnable_m85CA157B9B0ECEC641DB4F28506FE456DAF562B9,
	ArrowStats_Start_m9306E8DEB76F28D798461E844AEBD8FA79BBAD99,
	ArrowStats_Update_mF97643EAE181D86207D4055B549EAA532F08F23C,
	ArrowStats_RandomDir_m097FB8FC53250C2A2BEED7EF0B0C2CCE316E7873,
	ArrowStats__ctor_m44A6A13BE4C29C7C576FD54B4E9BAD7BFDF7D9FE,
	Border_OnTriggerEnter2D_mF00A6CA6D75E8C330ECDDD95389E1125C9529256,
	Border__ctor_mC47B2974B694148D0553A8B1BA49C1C06A4CB974,
	ChangeMenu_Change_mB9D17789278243192BDDE3F04EFBCFBC1DD4A5B3,
	ChangeMenu_ClearTutor_m51B62CD977BA017AB8DE25E3033B6C2A27C51A51,
	ChangeMenu__ctor_m8590CABB0EE49A4F89D6AE45EAABA1929A2C85AF,
	ChangeVolume_SetSound_m7C9168882B9895E62C1018BCE3CD15EC74721193,
	ChangeVolume_SetMusic_m0D15100E9B1178AAC34CD8C7D4969605E36527BF,
	ChangeVolume__ctor_mEF39DA9624C132587BC057E45EDA63FFB21968F6,
	Counter_Start_m77A2B23760AA7DA12C9BBEE742221AC01B67AFB3,
	Counter_Update_m750B99E43903C32501752EDB091B76D4D8591063,
	Counter_AnimalCollected_m350E7A131AFE7EB7EC5619D2F854469B23C6A94D,
	Counter__ctor_m12F27AA8C9EDE176E9543076DB214639246AAC73,
	Hook_Start_m24B908B8FD7C19B750AC841EE5FAE4A36339A4F1,
	Hook_Update_m754647819A7215B7620FF44950F52962DB13FD76,
	Hook_LaunchAnimal_m4A87E70FB9E4B19A7EFF82E506A0FCFF95943ED3,
	Hook_AnimalSetUp_m7197382F476EA370FA9F2BDA2F4B851B497ECA34,
	Hook_OnTriggerEnter2D_m29375DE9BB6C0820DDA4B0AA450BAEDC4DFE4347,
	Hook_SetAnimalPaternt_m2253BC223C7FD0DFF8372BBD4DDFB9EA30CC6344,
	Hook__ctor_m693B9FDDE2C931A45DDC1B9A1C28EB7915678254,
	U3CSetAnimalPaterntU3Ed__25__ctor_mB742D425984D061D0168939974145056BD1DC1B3,
	U3CSetAnimalPaterntU3Ed__25_System_IDisposable_Dispose_mC9141234784CC5EEDBDE65696C3DB05986773A03,
	U3CSetAnimalPaterntU3Ed__25_MoveNext_m653F8015AD50EE757BABDF3C5968F6FA15FA2739,
	U3CSetAnimalPaterntU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE1A64F7ADA0306BA74D80C6DBDEE29BB21E1B21,
	U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_Reset_mB0193444DB856035BD3981DE8F420A1B20BED1F4,
	U3CSetAnimalPaterntU3Ed__25_System_Collections_IEnumerator_get_Current_mEA7F2410B6D6EB7169A167D765272A2284E3BD98,
	ImageAdder_Awake_m9E8C22521946B3CCA47744E7F7C40900BE67FA1D,
	ImageAdder_Start_m458792536E8897B7C4E56278B5EC120A28E89BE4,
	ImageAdder_SetValue_mA5A8358C7F021A158DB9C2416EB363BBDA478550,
	ImageAdder_RemoveImage_mDAE0E516DA388FB5D4EFE3C8DA48194C8A5AF7DA,
	ImageAdder_Update_m35A08A2C257A766E998BDF05BCFC7F570CF1987F,
	ImageAdder__ctor_m6709E21B8CDF0D20E75DC9E46607ABDC6A4E3B4A,
	Lives_Update_m4FF28CBFFB9B3F0A5E85BF9308691C8682AFF75D,
	Lives_ChangeHealth_mE2A807665CD8C1A4B8FAE8D11DC054164CE391E3,
	Lives__ctor_m77E2BF995DBECEBFB9B04F92EE9C0863AC438AEC,
	ReloadScene_Start_mC5ABE315E40286FE314F4F60294A4FA5A5EEE540,
	ReloadScene_Update_mB265B9700FC2A03B8C5C3D496FF6FC3B070E16D2,
	ReloadScene_LoadScene_mB265DE58BB90E668D1369936550B4439184E540B,
	ReloadScene_ReLoad_mC5D67DAECB09026B8AAE83DAF21510F6128DE45E,
	ReloadScene_GameTime_m36814128F79BEB3040EE91E57A9927F31E7B0E5D,
	ReloadScene__ctor_mDDA626CD723DA4B7EE5BD4F0DA2CCAADF3F2081E,
	Rope_Awake_m5E84CC9E098DDCC82AB23D409A275D52D03A99B9,
	Rope_SetUpLine_mE457D644AFEA7E498BF232ED5FABB98E00F227C2,
	Rope_Update_mC94A4912E91FE4D823146889234424AA1D24D393,
	Rope__ctor_m08A38A25419AAB806FB62BB0DC83606EB1EE518D,
	RopeHost_Start_m93F757227F9067512D6D553C588BF33AA66C9F45,
	RopeHost_Update_mFD013478A3639ACB8BB6DE48A41D1B9A7B192527,
	RopeHost__ctor_mA80C48A6A1E2513DB360F7291CBBEC3CEA95E23D,
	SpawnArrows_Start_m6F85F93489E8C02F4D7281A8577AF3823331D616,
	SpawnArrows_Update_m94D1056DE5397B5A815FED5968001853351A4E76,
	SpawnArrows_GetPressedButton_m0966981F8F7708C38832CD6550CA4267CC4C1361,
	SpawnArrows_SetLastArrowActive_m2001D319F6199B46BB817262C9FB63AD0341C510,
	SpawnArrows_SetAmountOfArrows_m25A454CB577A1333F928117357B0D17FC998BB8E,
	SpawnArrows_ResetArrowsValues_mE6914F631A10EACEC79678DEE895E8CD11C75DE3,
	SpawnArrows__ctor_m4F3DA99697B9F6AA06A4B88A3B6584BB95674FB9,
	Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8,
	Spawner_Update_m8E44DB2210E6C1692B202D38D0867961E9720AA9,
	Spawner_ResetChances_mA8C792419BBF97274D2E75B89F8B3D65E6F6C8AD,
	Spawner_SpawnAnimal_mE780A4641654B1587610AF4A62A070E4BB681163,
	Spawner_WaitAndPrint_m84781AD3E91DCCE5F55370470FEB6EDAAD7A90D6,
	Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C,
	U3CWaitAndPrintU3Ed__14__ctor_m13628828E6174F666C9D54AF360AA61A53B8FF06,
	U3CWaitAndPrintU3Ed__14_System_IDisposable_Dispose_m1898DEDB299AE15F0E53D5DFAB25C67808A8B5E7,
	U3CWaitAndPrintU3Ed__14_MoveNext_m92DD1C54A5A036567CC200546B38A727BD2E784E,
	U3CWaitAndPrintU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4443DD1EE085AE8E2853718C0F4D215560CA8E71,
	U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_Reset_m5F0E9697F961B13CC552FD329B4005C8AF9F76DE,
	U3CWaitAndPrintU3Ed__14_System_Collections_IEnumerator_get_Current_mABEF781BFDB876A1E0EE81333EA325EFAFD974E0,
	Timer_Start_mF356578A412E0D4BE318BC567718C7DBD90E05F0,
	Timer_OnEnable_m46932BE65908389800B77332F5F6FDA0BF3C596B,
	Timer_OnDisable_mD522C218198DAA511E56B09CABB7EA58022E8FCB,
	Timer_SetTime_m2C010FEA2C0A8AACB10D45D4829A1AA8D2A8FC83,
	Timer_Update_m336594DF06E073C9CC317142E46E02AFC94A026C,
	Timer_ChangePitch_m27DC73EAF0836A0D0ED3567498BAC3850E8C5ED5,
	Timer_TimerStart_m2A9D651E9D42FA173C24B884EB81F3D181D6D558,
	Timer__ctor_m74709038BC88FE71F7D6C06D0FF352FBE17410E8,
	U3CChangePitchU3Ed__11__ctor_m2801AE84732F6D9E51B98C1C637A4C32B9652F5D,
	U3CChangePitchU3Ed__11_System_IDisposable_Dispose_mE46803F6C93BACD92C598F801527C0F2C89B7D3E,
	U3CChangePitchU3Ed__11_MoveNext_m798294331D66B49C937F5AD740DD86A92EDE9023,
	U3CChangePitchU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BBB9674CB3908CC7B7FB10F4B694887B688E838,
	U3CChangePitchU3Ed__11_System_Collections_IEnumerator_Reset_mB0435CFEF66344023B0C95E4D331B7443E97BB86,
	U3CChangePitchU3Ed__11_System_Collections_IEnumerator_get_Current_mCE2E20C669A79B9F2BB72CCDA8B9E4F83B3EE9F3,
	U3CTimerStartU3Ed__12__ctor_m1C21FE74F4545C01E22A3A35B87ABDD93BB0E1FB,
	U3CTimerStartU3Ed__12_System_IDisposable_Dispose_m1C4A3195C7A57ADC4D8985619F95C2F7D8067660,
	U3CTimerStartU3Ed__12_MoveNext_m65C48DA04C661F798E970AC86A070BE95B970AB6,
	U3CTimerStartU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF8F9C60991ACF051917A4A516E6923E41C31EFD,
	U3CTimerStartU3Ed__12_System_Collections_IEnumerator_Reset_mB691905ECD7D3D98AD081479657EABBDC25EF95F,
	U3CTimerStartU3Ed__12_System_Collections_IEnumerator_get_Current_m05419DFA7E918EAEE2DDB0EEF141D75587276483,
	Walk_Start_m09A7D2B2F39B6784AAE0BE9D285D04A5CD593585,
	Walk_OnEnable_mE9BDC27A7B11ABB72556F87FFE85E9F62A1F9C95,
	Walk_OnDisable_mFE7BC58553D49A6BC3C7D1E3392D74E1AE1987B1,
	Walk_Update_m574B5C3EE3EAE9BD3F44BB5F1CF1751D40E84468,
	Walk_OnTriggerEnter2D_mC911A427400A6F72CC308F28E9E86E037B3867C4,
	Walk_OnTriggerStay2D_mA14677244C152E766D3B96F35D98F50507217411,
	Walk_OnTriggerExit2D_mF49820AA9887920809700E429419FE9965FAFD0C,
	Walk__ctor_m0071C19F2A207DCFA448CA24805BF25D39A014F7,
};
static const int32_t s_InvokerIndices[99] = 
{
	1840,
	1840,
	1840,
	1840,
	1840,
	1532,
	1840,
	1840,
	1840,
	1840,
	1554,
	1554,
	1840,
	1840,
	1840,
	1519,
	1840,
	1840,
	1840,
	1840,
	605,
	1532,
	1206,
	1840,
	1519,
	1840,
	1817,
	1791,
	1840,
	1791,
	1840,
	1840,
	1519,
	1519,
	1840,
	1840,
	1840,
	1840,
	1840,
	1840,
	1840,
	1519,
	1840,
	1552,
	1840,
	1840,
	1532,
	1840,
	1840,
	1840,
	1840,
	1840,
	1840,
	1840,
	1532,
	1840,
	1519,
	1840,
	1840,
	1840,
	1840,
	1840,
	1840,
	1208,
	1840,
	1519,
	1840,
	1817,
	1791,
	1840,
	1791,
	1840,
	1840,
	1840,
	1554,
	1840,
	1208,
	1208,
	1840,
	1519,
	1840,
	1817,
	1791,
	1840,
	1791,
	1519,
	1840,
	1817,
	1791,
	1840,
	1791,
	1840,
	1840,
	1840,
	1840,
	1532,
	1532,
	1532,
	1840,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	99,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
